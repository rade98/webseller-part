<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Proxy_model extends CI_Model {

	/**
	 * @var Memcached $mem_var
	 */
	private $mem_var;

	/**
	 * @brief  Calling function that assaing instance of Memcached.
	 * 
	 * @return void
	 */
	public function __construct() {
		parent::__construct();
		$this->load->helper("memcached");
		$this->initMemcached();
	}

	/**
	 * @brief  Get the oldest used proxy from the Memcached, if the Memcached is empty load data from
	 *         the DB. If the Memcached it is not set will get the data from the DB.	
	 *
	 * @param  string $story_name The key in the Memcached that holds the proxies used by specific store.	
	 *
	 * @return array
	 */
	public function getProxyFromMemcached($store_name) {
		
		if (!isset($this->mem_var)) {
			return $this->getDataFromDB();
		}

		$store_data = unserialize($this->mem_var->get($store_name));

		if ($store_data == false) {
			$store_data = $this->fillMemcachedFromDB($store_name);
		}
		
		usort($store_data, "custom_sort_last_used");

		return $store_data[0];
	}

	/**
	 * @brief  Get the oldest used proxy, update it's last used date and put it back in the Memcached
	 *         by the store key for the Memcached	
	 *
	 * @param  string $story_name The key in the Memcached that holds the proxies used by specific store.	
	 *
	 * @return array
	 */
	public function getData($store_name = false) {
		
		if($store_name==false){
			$store_name = "all";
		}
		
		$older_used_proxy = $this->getProxyFromMemcached($store_name);

		$store_proxies = unserialize($this->mem_var->get($store_name));

		foreach ($store_proxies as &$proxy) {
			
			if ($older_used_proxy['id'] == $proxy['id']) {

				$proxy['last_used'] = date('Y-m-d H:i:s');
			}
		}

		$this->mem_var->set($store_name, serialize($store_proxies), 0);
		
		if(empty($older_used_proxy)){
			$ci =& get_instance();
			$ci->load->model("Proxyold_model","proxyold_model");
			$older_used_proxy = $ci->proxyold_model->getData();
		}
		
		return $older_used_proxy;
	}

	/**
	 * @brief  Update proxy by given id.	
	 *
	 * @param  int $id The proxy id in the DB.	
	 *
	 * @return void
	 */
	public function updateDB($id) {

		$this->db->update("proxy_ips", array("last_used" => date('Y-m-d H:i:s')), array('id' => $id));
	}

	/**
	 * @brief  Fill the Memcached if it's empty with the all data from the proxy_ips table with the keys
	 *         for every store from the stores table.	
	 *
	 * @param  string $story_name The key in the Memcached that holds the proxies used by specific store.	
	 *
	 * @return array
	 */
	public function fillMemcachedFromDB($store_name) {
		
		$all_proxies = $this->db->select()->from("proxy_ips")->get()->result_array();
		$all_stores = $this->db->from("stores")->get()->result_array();
	
		if($store_name=="all"){
			$this->mem_var->set($store_name, serialize($all_proxies), 0);
		}
		
		foreach($all_stores as $store){
			$this->mem_var->set($store['internal_name'], serialize($all_proxies), 0);
		}
		
		return unserialize($this->mem_var->get($store_name));
	}

	/**
	 * @brief  If the Memcached it is not set by some reason will be used the DB.	
	 *
	 * @return array
	 */
	public function getDataFromDB() {
	
		$row = $this->db->from("proxy_ips")
						->where("marked_as_timeout",0)
						->order_by("last_used", "asc")
						->limit(1)
						->get()
						->row_array();
		
		$this->db->update("proxy_ips", array("last_used" => date('Y-m-d H:i:s')), array('id' => $row['id']));
		
		return $row;
	}

	public function markErrored($id) {
		$this->db->update("proxy_ips", array("last_error" => date('Y-m-d H:i:s')), array('id' => $id));
	}

	/**
	 * @brief  Instantiate the Memcached object and add server.	
	 *
	 * @return void
	 */
	private function initMemcached() {
		
		try{
			
			$this->config->load('memcached');
			$defaultMemcached = $this->config->item('default');
			
			$this->mem_var = new Memcached();
			$this->mem_var->addServer($defaultMemcached['hostname'], $defaultMemcached['port']);
			
		} catch (Exception $e){
			echo 'Nqma instaliran memcache na php-to.';
			return;
		}
		
	}
}

<?php
defined('BASEPATH') or exit('No direct script access allowed');

require_once ("interfaces/IStoreHandler.php");

class Sears_handler implements IStoreHandler {

	private $error = "";

	private $apikey;

	private $store = "Sears";

	public function __construct($data) {
		
		if (isset($data['json'])) {
			$this->setApiKey($data['json']);
		}
		
		if (isset($data['store'])) {
			if($data['store']['friendly_name'] == "Sears.com"){
				$this->store = "Sears";
			}
			if($data['store']['friendly_name'] == "Kmart.com"){
				$this->store = "Kmart";
			}
		}
		
	}

	public function setApiKey($key) {
		
		$keyObj = json_decode($key);
		
		if (!isset($keyObj->apikey)) {
		/*	$this->error = "Not founded avalible api keys... Please, contact support."; */
			$this->apikey = "iac001FCD78F01C3E141973302A4E28243D40E05112009";
			return $this->apikey;
		} else {
			$this->apikey = $keyObj->apikey;
			return $this->apikey;
		}
	}
	
	public function setStore($storeData) {
		
		if($storeData['friendly_name'] == "Sears.com"){
			$this->store = "Sears";
		}
		
		if($storeData['friendly_name'] == "Kmart.com"){
			$this->store = "Kmart";
		}
	}
	
	private function checkOOSBySearch($product_id, $store, $variation){
		
		// Parser check OOS
		libxml_use_internal_errors(true);
		
		$this->CI = & get_instance();
		$this->CI->load->helper("ProxyCURL");
		$this->CI->load->model("Proxy_model", "proxy", TRUE);
		
		$store = strtolower($store);
		
		if ($store == "sears") {
			$request_url = "http://www.sears.com/service/search/v2/productSearch?catalogId=12605&keyword=$product_id&rmMattressBundle=true&searchBy=keyword&storeId=10153&tabClicked=All&visitorId=Test";
		}
		if ($store == "kmart") {
			$request_url = "http://www.kmart.com/service/search/v2/productSearch?catalogId=12605&keyword=$product_id&rmMattressBundle=true&searchBy=keyword&storeId=10153&tabClicked=All&visitorId=Test";
		}
		
		$proxyData = $this->CI->proxy->getData();
		if (empty($proxyData)) {
			$this->error .= "Cant get proxy list.";
		}
		
		$backData = getDataProxyCURL($proxyData['ip'], $proxyData['port'], $proxyData['username'], $proxyData['password'], $request_url);
		
		if ($backData['error'] == true) {
			$this->error .= "Proxy error:" . $backData['error'];
			$this->CI->proxy->markErrored($proxyData['id']);
			
			return true;
			
		} else {
			
			$json = json_decode($backData['result'], TRUE);
			
			if($json['pageType'] == "KEYWORD_NO_RESULT"){
				return true;
			}
		}
		
	}
	
	private function checkOOS($product_id, $store, $variation) {
		
		// Parser check OOS
		libxml_use_internal_errors(true);
		
		$this->CI = & get_instance();
		$this->CI->load->helper("ProxyCURL");
		$this->CI->load->model("Proxy_model", "proxy", TRUE);
		
		$store = strtolower($store);
		
		if ($store == "sears") {
			$request_url = "http://www.sears.com/content/pdp/config/products/v1/products/$product_id?site=sears&type=international";
		}
		if ($store == "kmart") {
			$request_url = "http://www.kmart.com/content/pdp/config/products/v1/products/$product_id?site=kmart&type=international";
		}
		
		if (isset($variation['variation']) == true) {
			$uid = $variation['data'];
			if ($store == "sears") {
				$request_url = "http://www.sears.com/content/pdp/config/products/v1/products/$product_id/variations/$uid/offers/topranked?site=sears&type=international";
			}
			if ($store == "kmart") {
				$request_url = "http://www.kmart.com/content/pdp/config/products/v1/products/$product_id/variations/$uid/offers/topranked?site=kmart&type=international";
			}
		}
		
		$proxyData = $this->CI->proxy->getData();
		if (empty($proxyData)) {
			$this->error .= "Cant get proxy list.";
		}
		
		$backData = getDataProxyCURL($proxyData['ip'], $proxyData['port'], $proxyData['username'], $proxyData['password'], $request_url);
		if ($backData['error'] == true) {
			$this->error .= "Proxy error:" . $backData['error'];
			$this->CI->proxy->markErrored($proxyData['id']);
			return true;
		} else {
			
			$json = json_decode($backData['result'], TRUE);
			
			if (! isset($json['data']['offermapping']['fulfillment']['shipping'])) {
				
				if (isset($json['data']['productstatus']['uid'])) {
					
					if ($store == "kmart") {
						$request_url = "http://www.kmart.com/content/pdp/v1/products/$product_id/variations/" . $json['data']['productstatus']['uid'] . "/ranked-sellers?site=kmart";
					}
					if ($store == "sears") {
						$request_url = "http://www.sears.com/content/pdp/v1/products/$product_id/variations/" . $json['data']['productstatus']['uid'] . "/ranked-sellers?site=sears";
					}
					
					$proxyData = $this->CI->proxy->getData();
					$backData = getDataProxyCURL($proxyData['ip'], $proxyData['port'], $proxyData['username'], $proxyData['password'], $request_url);
					$json = json_decode($backData['result'], TRUE);
					
					$json_shipping    = $json['data']['Offer']['offermapping']['fulfillment']['shipping'];
					$json_delivery    = $json['data']['Offer']['offermapping']['fulfillment']['delivery'];
					$json_storepickup = $json['data']['Offer']['offermapping']['fulfillment']['storepickup'];
					
					if(isset($json['data']['offerstatus'])){
						$json_offer_status = $json['data']['offerstatus']['canDisplay'];
					}else{
						$json_offer_status = false;
					}
					
					return $this->getOOSLogic($json_shipping, $json_delivery,$json_storepickup,$json_offer_status);
					
				} else {
					return true;
				}
			}
			
			$json_shipping    = $json['data']['offermapping']['fulfillment']['shipping'];
			$json_delivery    = $json['data']['offermapping']['fulfillment']['delivery'];
			
			if(isset($json['data']['Offer']['offermapping']['fulfillment']['storepickup'])){
				$json_storepickup = $json['data']['Offer']['offermapping']['fulfillment']['storepickup'];
			}else{
				$json_storepickup = false;
			}
				
			$json_offer_status = $json['data']['offerstatus']['canDisplay'];
			
			return $this->getOOSLogic($json_shipping, $json_delivery, $json_storepickup, $json_offer_status);
		}
	}
	
	private function getOOSLogic($json_shipping, $json_delivery, $json_storepickup, $json_offer_status){
		
		/*
		var_dump($json_shipping);
		var_dump($json_delivery);
		var_dump($json_storepickup);
		var_dump($json_offer_status);
		*/
		
		if ($json_shipping == true && $json_delivery == false && $json_storepickup == false && $json_offer_status == false) {
			return true;
		}
		
		if ($json_shipping == true && $json_delivery == false && $json_storepickup == false) {
			return false;
		}
		
		if ($json_shipping == true && $json_delivery == true && $json_offer_status == true) {
			return false;
		}
		
		if ($json_shipping == true && $json_delivery == false && $json_storepickup == true) {
			return false;
		}
		
		if ($json_shipping == true && $json_delivery == false && $json_offer_status == true) {
			return false;
		} 
		
		if ($json_shipping == false && $json_delivery == true && $json_offer_status == true) {
			return false;
		} 
		
		if ($json_shipping == true && $json_delivery == true && $json_offer_status == false) {
			return true;
		}
		
		if ($json_shipping == false && $json_delivery == false && $json_offer_status == false) {
			return true;
		} 
		
		if ($json_shipping == true && $json_delivery == false) {
			return true;
		}
		
		return true;
		
	}

	private function getPriceFromAjaxDisplay($product_id, $offer_id = NULL) {
		
		$this->CI = & get_instance();
		$this->CI->load->library('guzzle');
		
		$error = '';
		
		$this->error = "";
		$store = $this->store;
		$store = strtolower($store);
		$store_up = strtoupper($store);
		$product_id = str_replace("#","",$product_id);
		$clean_product_id = str_replace("P","",$product_id);
		
		if ($store == "sears") {
			$request_url = "http://www.sears.com/content/pdp/products/pricing/v2/get/price/display/json?offer=$clean_product_id&priceMatch=Y&memberType=G&site=$store_up";
		}
		
		if($store == "kmart") {
			$request_url = "http://www.kmart.com/content/pdp/products/pricing/v2/get/price/display/json?offer=$clean_product_id&priceMatch=Y&memberType=G&site=$store_up";
		}
	
		try {
			
			$client = new GuzzleHttp\Client(['headers'=>[
			  'Content-Type'=>'application/json',
			  'AuthID'=>'aA0NvvAIrVJY0vXTc99mQQ==',
			  'User-Agent'=>'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:51.0) Gecko/20100101 Firefox/51.0'
			]]);
			
			$response = $client->request('GET', $request_url);
		
			$json_info = $response->getBody()->getContents();
			$json_info = json_decode($json_info, TRUE);
			
			return array("price"=>$json_info['priceDisplay']['response'][0]['finalPrice']['numeric']); 
			
		} catch (GuzzleHttp\Exception\BadResponseException $e) {
			return array();	
		}
	}
	
	private function getPriceFromAjax($product_id,$offer_id = NULL) {
		
		$this->CI = & get_instance();
		$this->CI->load->library('guzzle');
		
		$error = '';
		
		$this->error = "";
		$store = $this->store;
		$store = strtolower($store);
		$store_up = strtoupper($store);
		$product_id = str_replace("#","",$product_id);
		
		if ($store == "sears") {
			$request_url = "http://www.sears.com/content/pdp/products/pricing/v2/post/price/json";
		}
		if($store == "kmart") {
			$request_url = "http://www.kmart.com/content/pdp/products/pricing/v2/post/price/json";
		}
	
		try {
			
			$client = new GuzzleHttp\Client(['headers'=>[
			  'Content-Type'=>'application/json',
			  'AuthID'=>'aA0NvvAIrVJY0vXTc99mQQ==',
			  'User-Agent'=>'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:51.0) Gecko/20100101 Firefox/51.0'
			]]);
			
			$clean_p_id = str_replace("P","",$product_id);
			
			if ($store == "sears"){
				$json = '{"price-request":{"site":"'.$store_up.'","price-match":"Y","zip-code":null,"storeunit-number":"","offer-info":[{"id":"'.$clean_p_id.'"}]}}';
			}
			if($store == "kmart"){
				$json = '{"price-request":{"site":"'.$store_up.'","price-match":"Y","zip-code":null,"storeunit-number":"","offer-info":[{"id":"'.$clean_p_id.'"}]}}';
			}
			
			$json = json_decode($json, TRUE);
			
			$response = $client->request('POST', $request_url, [
				'json'=> $json
			]);
			
			 
			$json_info = $response->getBody()->getContents();
			$json_info = json_decode($json_info, TRUE);
			
			if(isset($json_info["price-response"]['item-response'][0]['offer'])){
				$offer_id = $json_info["price-response"]['item-response'][0]['offer'];
				//return $this->getPriceFromAjax($offer_id);
			}
			
			return array("price"=>$json_info['price-response']['item-response'][0]['sell-price']['price']); 
			
		} catch (GuzzleHttp\Exception\BadResponseException $e) {
			return array();	
		}
	}
	
	private function getProductDetailsVariationFromAjax($product_id, $product_uid){
		
		libxml_use_internal_errors(true);
		
		$this->CI = & get_instance();
		$this->CI->load->library('guzzle');
		$this->CI->load->helper("ProxyCURL");
		$this->CI->load->model("Proxy_model", "proxy", TRUE);
		
		$error = '';
		
		$this->error = "";
		$store = $this->store;
		$store = strtolower($store);
		$store_up = strtoupper($store);
		$product_id = str_replace("#","",$product_id);
		
		if ($store == "sears") {
			$request_url = "http://www.sears.com/content/pdp/v1/products/$product_id/variations/$product_uid/ranked-sellers?site=sears&memberStatus=G&sywMax=N";
		}
		if ($store == "kmart") {
			$request_url = "http://www.kmart.com/content/pdp/v1/products/$product_id/variations/$product_uid/ranked-sellers?site=kmart&memberStatus=G&sywMax=N";
		}
		$proxyData = $this->CI->proxy->getData();
		if (empty($proxyData)) {
			$this->error .= "Cant get proxy list.";
		}
		
		$backData = getDataProxyCURL($proxyData['ip'], $proxyData['port'], $proxyData['username'], $proxyData['password'], $request_url);
		
		if ($backData['error'] == true) {
			$this->error .= "Proxy error:" . $backData['error'];
			$this->CI->proxy->markErrored($proxyData['id']);
			return true;
		} else {
			
			$data = array();
			$json = json_decode($backData['result'], TRUE);
			var_dump($json);
		
		}
	}
	
	private function getProductDetailsFromAjax($product_id){
		
		libxml_use_internal_errors(true);
		
		$this->CI = & get_instance();
		$this->CI->load->library('guzzle');
		$this->CI->load->helper("ProxyCURL");
		$this->CI->load->model("Proxy_model", "proxy", TRUE);
		
		$error = '';
		
		$this->error = "";
		$store = $this->store;
		$store = strtolower($store);
		$store_up = strtoupper($store);
		$product_id = str_replace("#","",$product_id);
		
		if ($store == "sears") {
			$request_url = "http://www.sears.com/content/pdp/config/products/v1/products/$product_id?site=sears";
		}
		if ($store == "kmart") {
			$request_url = "http://www.kmart.com/content/pdp/config/products/v1/products/$product_id?site=kmart";
		}
		$proxyData = $this->CI->proxy->getData();
		if (empty($proxyData)) {
			$this->error .= "Cant get proxy list.";
		}
		
		$backData = getDataProxyCURL($proxyData['ip'], $proxyData['port'], $proxyData['username'], $proxyData['password'], $request_url);
		
		if ($backData['error'] == true) {
			$this->error .= "Proxy error:" . $backData['error'];
			$this->CI->proxy->markErrored($proxyData['id']);
			return true;
		} else {
			
			$data = array();
			$json = json_decode($backData['result'], TRUE);
			
			if(isset($json['data']['productstatus']['uid'])){
				$product_uid = $json['data']['productstatus']['uid'];
				//$this->getProductDetailsVariationFromAjax($product_id, $product_uid);
			}
			
			$data['url'] = "http://www.$store.com/wsg/p-".$product_id;
			$data['title'] = $json["data"]["product"]["seo"]["title"];
			//$data['description'] = $json["data"]["product"]["seo"]["desc"];
			$data['image'] = $json['data']['product']['assets']['imgs'][0]['vals'][0]['src'];
			$data['description'] = $json['data']['product']['desc'][0]['val'];
			
			if(isset($json['data']['product']['brand']['name'])){
				$data['brand'] = $json['data']['product']['brand']['name'];
			}
			
			if(empty($json['data']['product']['assets']['imgs'][1])){
				$imagesArray[] = $data['image'];
			} else {
				foreach($json['data']['product']['assets']['imgs'][1] as $images){
					if(is_array($images)){
						foreach($images as $img){
							$imagesArray[] = $img['src'];
						}
					}
				}
			}
			
			$data['images'] = $imagesArray;

			return $data;
		}
	}
	
	public function whenApiIsDie($product_id){
		
		$i=0;
		
		$product_id = str_replace("#","",$product_id);
		$product_id = str_replace("_","",$product_id);
		
		$productDetails = $this->getProductDetailsFromAjax($product_id);
		//$priceDetails = $this->getPriceFromAjax($product_id);
		$priceDetails = $this->getPriceFromAjaxDisplay($product_id);
		
		if($priceDetails['price'] == NULL){ 
			return NULL;
		}
		
		$out[$i]['url'] = $productDetails['url'];
		$out[$i]['id'] = $product_id;
		$out[$i]['uid'] = 'None';
		$out[$i]['price'] = $priceDetails['price'];
		$out[$i]['variant'] = "None";
		$out[$i]['name'] = $productDetails['title'];
		$out[$i]['image'] = $productDetails['image'];
		
		//check product is oos
		if ($this->checkOOS($product_id, $this->store, array()) == true) {
			$out[$i]['inStock'] = false;
		}else{
			$out[$i]['inStock'] = true;
		}
		
		//check product is oos #2
		if ($this->checkOOSBySearch($product_id, $this->store, array()) == true) {
			$out[$i]['inStock'] = false;
		}else{
			$out[$i]['inStock'] = true;
		}
		
		// adding images
		$out[$i]['more_details'] = array();
		if (isset($productDetails['images'])) {
			$i_img_g = 0;
			foreach($productDetails['images'] as $imgGall) {
				$out[$i]['more_details']['image_gallery'][$i_img_g]['imageURL'] = $imgGall;
				$i_img_g++;
			}
		}
		
		$out[$i]['more_details']['description'] = $productDetails['description'];
		$out[$i]['more_details']['brand'] = $productDetails['brand'];
		
		return $out;
	}
	
	private function sears_query_json($partNumber, $type) {
		
		$error = '';
		
		if ($type == "id") {
			$type = "getProduct";
		} elseif ($type == "uid") {
			$type = "getOfferByUid";
		}
		
		$this->error = "";
		$partNumber = str_replace("#","",$partNumber);
		
		//$request_url = "http://api.developer.sears.com/v3/productDetail/{$type}/{$this->store}/json/{$partNumber}?apikey=" . $this->apikey;
		$request_url = "http://api.sears.com/shcapi/v3/productDetail/{$type}/{$this->store}/json/{$partNumber}?appID=DEV_PORTAL_PRICES_COMPARE&authID=" . $this->apikey;
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $request_url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($ch);
		
		if (curl_errno($ch)) {
			$error = "HTTP error: " . curl_error($ch);
			curl_close($ch);
		} else {
			$json = json_decode($response);
			
			if ($json == NULL) {
				$le = error_get_last();
				$error = "Response could not be parsed. Try again later. Error information: " . $le['message'];
			} else {
				
				if (! empty($json->fault->faultstring)) {
					$error = $json->fault->faultstring . '. Please, contact support.';
				} else {
					$sears_error_1 = isset($json->statusData->ResponseCode) ? @trim($json->statusData->RespMessage) : "";
					
					if (empty($sears_error_1)) {
						curl_close($ch);
						return $json->SoftHardProductDetails;
					} else {
						$error_sum = $sears_error_1;
						$error = "Something went wrong. <br> <b>Response</b>: " . $error_sum;
					}
				}
			}
		}
		curl_close($ch);
		$this->error = $error;
		return false;
	}

	public function searchProducts($keyword, $category, $storeName, $sortBy, $min_priceSEG, $max_priceSEG) {
		$data = array();
		$keyword = urlencode($keyword);
		$vars = '';
		
		// PRICE_LOW_TO_HIGH
		// PRICE_HIGH_TO_LOW
		// RATING_HIGH_TO_LOW
		
		if (isset($sortBy) && ! empty($sortBy)) {
			if ($sortBy == "price_low") {
				$vars .= "&sortBy=PRICE_LOW_TO_HIGH";
			} elseif ($sortBy == "price_high") {
				$vars .= "&sortBy=PRICE_HIGH_TO_LOW";
			} else if ($sortBy == "rating_high") {
				$vars .= "&sortBy=RATING_HIGH_TO_LOW";
			}
		}
		// filter=price|50$-100^brand|Sony|
		if (empty($min_priceSEG)) {
			$min_priceSEG = 1;
		}
		if (empty($max_priceSEG)) {
			$max_priceSEG = 10000;
		}
		$vars .= "&filter=price|$min_priceSEG-$max_priceSEG";
		// $vars .= "&startIndex=3";
		$vars .= "&endIndex=1000";
		// echo $vars;
		$request_url = "http://api.developer.sears.com/v2.1/products/search/Sears/json/keyword/" . $keyword . "?apikey=" . $this->apikey . $vars;
		// echo $request_url;
		$response = @file_get_contents($request_url);
		if ($response === false) {
			$data['totalResults'] = null;
			$data['items'] = null;
			$error = "Request failed. Try again later.";
		} else {
			$json = json_decode($response);
			// var_dump($json);
			if ($json->SearchResults->ProductCount > 0) {
				$i = 0;
				foreach ($json->SearchResults->Products as $item) {
					if (! empty($item->Price->DisplayPrice)) {
						$items[$i]['itemID'] = $item->Id->PartNumber;
						$items[$i]['itemName'] = $item->Description->Name;
						$items[$i]['salePrice'] = isset($item->Price->DisplayPrice) ? $item->Price->DisplayPrice : '0.00';
						
						if (isset($item->Description->ImageURL)) {
							if ($item->Description->ImageURL == 'http://s7.sears.com/is/image/Sears/NoCat?$main$') {
								$items[$i]['itemImage'] = './assets/global/img/not_available.gif';
							} else {
								$items[$i]['itemImage'] = $item->Description->ImageURL;
							}
						} else {
							$items[$i]['itemImage'] = './assets/global/img/not_available.gif';
						}
						$items[$i]['itemRating'] = isset($item->Description->ReviewRating->Rating) ? $item->Description->ReviewRating->Rating : '0';
						$items[$i]['itemReview'] = isset($item->Description->ReviewRating->NumReview) ? $item->Description->ReviewRating->NumReview : '0';
						$items[$i]['itemStock'] = isset($item->Availability->StockIndicator) ? true : false;
						$i ++;
					}
				}
				$data['totalResults'] = $json->SearchResults->ProductCount;
				$data['items'] = $items;
			} else {
				$data['totalResults'] = null;
				$data['items'] = null;
			}
		}
		return $data;
	}

	public function getCategories() {
		$data = array(
			
			"1020003"=>"Appliances",
			"1020004"=>"Baby",
			"1023303"=>"Beauty",
			"1325032343"=>"Clothing, Shoes & Jewelry",
			"1020006"=>"Fitness & Sports",
			"1348654256"=>"Home",
			"1270629454"=>"Home Improvement",
			"1020001"=>"Lawn & Garden",
			"1024037"=>"Outdoor Living",
			"1020000"=>"Tools",
			"1020010"=>"Toys & Games"
		);
		
		return $data;
	}
	
	private function saveCacheImage($product_id, $image){
		
		$i_img_c = 1600;
		
		$file = "cache/images/sears-image-".$product_id.'-'.$i_img_c.'.jpg';
		
		if(!is_file($file)){
			
			$Imagick = new Imagick();
			$Imagick->readImage($image);
			$ImageGeometry = $Imagick->getImageGeometry();
			
			if($ImageGeometry['width']>1600){
				
				$Imagick->scaleImage(1600, 0);
				$Imagick->setImageCompression(Imagick::COMPRESSION_JPEG); 
				$Imagick->setImageCompressionQuality(100); 
				$Imagick->writeImage($file);
				$Imagick->clear();
				$Imagick->destroy();
				
			}
			
			//file_put_contents($file, file_get_contents($image));
		}
		
		return base_url() . "cache/images/sears-image-" . $product_id.'-' . $i_img_c.'.jpg';
		
	}

	public function getProduct($getID) {
		
		$out = array();
		$extra = array();
		
		$getID = explode('_', $getID);
		$id = $getID[0];
		
		if (isset($getID[1])) {
			$uid = $getID[1];
		} else {
			$uid = null;
		}
		
		$childJson = $this->sears_query_json($id, 'id');
		
		if($childJson == NULL){
			
			return false;
			/*
			$productSingle = array();
			$productFromProxy = $this->whenApiIsDie($id);
			$productFromProxy = $productFromProxy[0];
			
			$productSingle['title'] = $productFromProxy['name'];
			$productSingle['price'] = $productFromProxy['price'];
			$productSingle['picture'] = $productFromProxy['image'];
			$productSingle['more_details'] = $productFromProxy['more_details'];
			$productSingle['url'] = $productFromProxy['url'];
			$productSingle['inStock'] = $productFromProxy['inStock'];
			$productSingle['currency'] = 'USD';
			
			return $productSingle;
			*/
		}
		
		if ($childJson === false) {
			return;
		}
		
		if (isset($childJson->ProductVariant)) {
			if ($childJson->ProductVariant == "VARIATION") {
				
				$out['variation'] = true;
				$extra = array();
				if (! empty($uid)) {
					$childJsonUID = $this->sears_query_json($uid, 'uid');
					
					$out['id'] = $id . '_' . $uid;
					$out['title'] = $childJsonUID->DescriptionName;
					
					if (isset($childJsonUID->upc)) {
						$out['upc'] = $childJsonUID->upc;
					} else {
						$out['upc'] = '';
					}
					
					$out['inStock'] = $this->getOOSApiLogic(0, $childJsonUID);
					
					/*
					if ($this->checkOOS($id, $this->store, array(
						"variation"=>true,
						"data"=>$uid
					)) == true) {
						$out['inStock'] = false;
					}
					*/
					
					$out['currency'] = "USD";
					$out['url'] = $childJson->ProductURL;
					$out['price'] = isset($childJsonUID->ProductVariants->prodList->product[0]->prodVarList->prodVar->skuList->Sku[0]->price) ? $childJsonUID->ProductVariants->prodList->product[0]->prodVarList->prodVar->skuList->Sku[0]->price : 0;
					
					if (! empty($childJsonUID->ProductVariants->prodList->product[0]->prodVarList->colorSwatchList->colorSwatch[0]->mainImageName)) {
						$out['picture'] = $childJsonUID->ProductVariants->prodList->product[0]->prodVarList->colorSwatchList->colorSwatch[0]->mainImageName;
					} else {
						$out['picture'] = $childJson->MainImageUrl;
					}
					
					$this->saveCacheImage($out['id'], $out['picture']);
					
					foreach ($childJson->ProductVariantsInfo->UIDs as $value) {
						if ($value->Id == $uid) {
							
							foreach ($value->UIDDefiningAttributes as $key => $value) {
								$extra[$value->Name] = $value->Value;
							}
						}
					}
					$out['extra_data'] = $extra;
				}
			} elseif ($childJson->ProductVariant == "NONVARIATION") {
				
				$out['variation'] = false;
				$out['id'] = $id;
				$out['title'] = $childJson->DescriptionName;
				$out['upc'] = $childJson->upc;
				$out['variant'] = "None";
				$out['currency'] = "USD";
				$out['price'] = $childJson->SalePrice;
				$out['picture'] = $this->saveCacheImage($id, $childJson->MainImageUrl);
				$out['inStock'] = $this->getOOSApiLogic(0, $childJson);
					
				/*
				if ($this->checkOOS($id, $this->store, array()) == true) {
					$out['inStock'] = false;
				}
				*/
				
				$out['url'] = $childJson->ProductURL;
				$out['extra_data'] = $extra;
				
				// adding images
				$out['more_details'] = array();
				if (isset($childJson->ImageURLs->ImageURL)) {
					$i_img_g = 0;
					foreach ($childJson->ImageURLs->ImageURL as $imgGall) {
						$out['more_details']['image_gallery'][$i_img_g]['imageURL'] = $this->saveCacheImage($id.'_g_'.$i_img_g, $imgGall);
						$i_img_g ++;
					}
				}
				// adding description
				if (isset($childJson->LongDescription) && ! empty($childJson->LongDescription)) {
					$out['more_details']['description'] = $childJson->LongDescription;
				}
			}
		}
		if(empty($out)){
			$this->error .= "Cant get product.";
			return;
		}
		return $out;
	}

	public function getVariations($id) {
		if (! empty($this->error)) {
			return array(
				"error"=>$this->error
			);
		} else {
			
			$json = $this->sears_query_json($id, 'id');
			
			if($json == NULL){
				
				return; 
				//return $this->whenApiIsDie($id);
			} 
						
			if (! empty($this->error)) {
				return array(
					"error"=>$this->error
				);
			} else {
				
				if ($json == null) {
					return null;
				} else {
					$out = array();
					
					if (isset($json->ProductVariant)) {
						if ($json->ProductVariant == "VARIATION") {
							$i = 0;
							$attributes = array();
							
							// uids
							if (! empty($json->ProductVariantsInfo->UIDs)) {
								foreach ($json->ProductVariantsInfo->UIDs as $value) {
									$out[$i]['id'] = $id . "_".$value->Id;
									$out[$i]['uid'] = $value->Id;
									
									$jsonUID = $this->sears_query_json($value->Id, 'uid');
									
									if (! empty($jsonUID)) {
										if (! empty($jsonUID->ProductVariants->prodList->product[0]->prodVarList->colorSwatchList->colorSwatch)) {
											$out[$i]['image'] = $jsonUID->ProductVariants->prodList->product[0]->prodVarList->colorSwatchList->colorSwatch[0]->mainImageName;
										} else {
											$out[$i]['image'] = $json->MainImageUrl;
										}
										$out[$i]['name'] = $jsonUID->ProductVariants->prodList->product[0]->prodVarList->prodVar->skuList->Sku[0]->name;
										$out[$i]['price'] = $jsonUID->ProductVariants->prodList->product[0]->prodVarList->prodVar->skuList->Sku[0]->price;
										
										$out[$i]['inStock'] = $this->getOOSApiLogic($i, $jsonUID);
										
										/*
										if ($this->checkOOS($id, $this->store, array(
											"variation"=>true,
											"data"=>$value->Id
										)) == true) {
											$out[$i]['inStock'] = false;
										}
										*/
									} else {
										$out[$i]['image'] = $json->MainImageUrl;
										$out[$i]['name'] = $json->DescriptionName;
										$out[$i]['price'] = $json->SalePrice;
										$out[$i]['inStock'] = false; 
									}
									if (isset($value->UIDDefiningAttributes)) {
										$attributes[$i] = $value->UIDDefiningAttributes;
										$out[$i]['variant'] = "";
									}
									$i ++;
								}
								
								foreach ($attributes as $key => $value) {
									foreach ($attributes[$key] as $value) {
										$out[$key]['variant'] .= $value->Name . ' : ' . $value->Value . '<br />';
									}
								}
							} else {
								$out = null;
							}
						} else {
							
							$out[0]['id'] = $id;
							$out[0]['uid'] = 'None';
							$out[0]['name'] = $json->DescriptionName;
							$out[0]['variant'] = "None";
							$out[0]['price'] = $json->SalePrice;
							$out[0]['image'] = $json->MainImageUrl;
							
							$out[0]['inStock'] = $this->getOOSApiLogic(0, $json);
							
							/*
							if ($this->checkOOS($id, $this->store, array()) == true) {
								$out[0]['inStock'] = false;
							}
							*/
							
						}
					}
					
					return $out;
				}
			}
		}
	}
	
	private function getOOSApiLogic($i, $jsonUID){
		
		$out[$i] = array();
		
		$out[$i]['inStock'] = false;
		
		if(empty($jsonUID->ArrivalMethods->ArrivalMethod)){
			if (isset($jsonUID->InStock) && $jsonUID->InStock == 1) {
				$out[$i]['inStock'] = true;
			}
			return $out[$i]['inStock'];
		}
		
		if (isset($jsonUID->ArrivalMethods->ArrivalMethod[0]) && $jsonUID->ArrivalMethods->ArrivalMethod[0] == "Ship") {
			if (isset($jsonUID->InStock) && $jsonUID->InStock == 1) {
				$out[$i]['inStock'] = true;
			}
		}
		
		if(isset($jsonUID->ArrivalMethods->ArrivalMethod[1]) && $jsonUID->ArrivalMethods->ArrivalMethod[1] == "Ship") {
			if (isset($jsonUID->InStock) && $jsonUID->InStock == 1) {
				$out[$i]['inStock'] = true;
			}
		}
		
		if(isset($jsonUID->ArrivalMethods->ArrivalMethod[2]) && $jsonUID->ArrivalMethods->ArrivalMethod[2] == "Ship") {
			if (isset($jsonUID->InStock) && $jsonUID->InStock == 1) {
				$out[$i]['inStock'] = true;
			}
		}
		
		if(isset($jsonUID->ArrivalMethods->ArrivalMethod[3]) && $jsonUID->ArrivalMethods->ArrivalMethod[3] == "Ship") {
			if (isset($jsonUID->InStock) && $jsonUID->InStock == 1) {
				$out[$i]['inStock'] = true;
			}
		}
		
		if(isset($jsonUID->ArrivalMethods->ArrivalMethod[0]) && $jsonUID->ArrivalMethods->ArrivalMethod[0] == "Delivery") {
			if (isset($jsonUID->InStock) && $jsonUID->InStock == 1) {
				$out[$i]['inStock'] = true;
			}
		} 
		
		if (isset($jsonUID->ArrivalMethods->ArrivalMethod[1]) && $jsonUID->ArrivalMethods->ArrivalMethod[1] == "Delivery") {
			if (isset($jsonUID->InStock) && $jsonUID->InStock == 1) {
				$out[$i]['inStock'] = true;
			}
		}
		
		if (isset($jsonUID->ArrivalMethods->ArrivalMethod[2]) && $jsonUID->ArrivalMethods->ArrivalMethod[2] == "Delivery") {
			if (isset($jsonUID->InStock) && $jsonUID->InStock == 1) {
				$out[$i]['inStock'] = true;
			}
		}
		
		if (isset($jsonUID->ArrivalMethods->ArrivalMethod[3]) && $jsonUID->ArrivalMethods->ArrivalMethod[3] == "Delivery") {
			if (isset($jsonUID->InStock) && $jsonUID->InStock == 1) {
				$out[$i]['inStock'] = true;
			}
		}
		
		return $out[$i]['inStock'];
		
	}

	public function getError() {
		return $this->error;
	}

	public function parseId($idOrURL) {
		$last = explode("/", $idOrURL);
		$last_end = end($last);
		
		if(empty($last_end)){
			$last_end = $last[sizeof($last)-2];
		}
		
		$id = explode("?", $last_end);
		$numid = explode("p-", $id[0]);
		
		
		if (! empty($numid[0])) {
			return $numid[0];
		} else {
			if (! empty($numid[1])) {
				return $numid[1];
			} else {
				return null;
			}
		}
	}
}

<?php
defined('BASEPATH') or exit('No direct script access allowed');

require_once ("interfaces/IStoreHandler.php");

class Walmart_handler implements IStoreHandler {
	
	private $error = "";
	private $apikey;
	
	private $ci;
	private $work_with_proxy = true;
	
	public function __construct($data) {
		
		if (isset($data['json'])) {
			$this->setApiKey($data['json']);
		}
		
		$this->ci = & get_instance();
		$this->ci->load->model("Proxy_model", "proxy", TRUE);
		$this->ci->load->helper("ProxyCURL");
		$this->ci->load->helper("Python");
		
		libxml_use_internal_errors(true);
	}
	
	public function setApiKey($key) {
		
		$keyObj = json_decode($key);
		
		if (empty($keyObj->apikey)) {
			$this->error = "Something went wrong. Please, contact support.";
		} else {
			$this->apikey = $keyObj->apikey;
			return $this->apikey;
		}
	}
	
	public function getPriceAndStock($id){
		
		$products = $this->getVariationsWithProxy($id);
		
		if(empty($products)){
			return array("stock"=>false,"log"=>"");
		}
		
		foreach($products as $product){
			if($product['id']==$id){
				return array("stock"=>$product['inStock'], "price"=>$product['productPrice'], "log"=>"");
				break;
			}
		}
		
		return array("stock"=>false, "price"=>"0.00", "log"=>"");
	}
	
	public function checkStockStatusNewMethod($id, $apiPrice){
		
		$products = $this->getVariationsWithProxy($id);
		
		if(empty($products)){
			return array("stock"=>false,"log"=>"");
		}
		
		foreach($products as $product){
			if($product['id']==$id){
				if(isset($product['productPrice']) && $product['productPrice'] == $apiPrice){
					if($product['inStock'] == true){
						return array("stock"=>true,"log"=>"");
					}
				}
				break;
			}
		}
		
		return array("stock"=>false, "log"=>"");
		
		/*
		 ///////////////////////////////////////////////////////////////////
		 
		 $stock_status = false;
		 
		 $url = "https://walmart.com/ip/".$id;
		 
		 $content = getContentWithProxy($url);
		 
		 if(!empty($content)){
		 $content['response'] = $content;
		 } else {
		 $content['response'] = null;
		 }
		 
		 if(empty($content['response'])){
		 return array("stock"=>$stock_status,"log"=>"");
		 }
		 
		 if(!empty($content['response'])){
		 
		 $str = str_replace(',"offers":{"ba', ',"offersBAD":{"ba', $content['response']);
		 $exp = explode(',"offers":{"',$str);
		 
		 if(!isset($exp[1])){
		 return array("stock"=>$stock_status,"log"=>"");
		 }
		 
		 $exp = explode(',"images":{"',$exp[1]);
		 $exp = '{"offers":{"'.$exp[0]."}";
		 $exp = json_decode($exp,TRUE);
		 
		 if(sizeof($exp['offers']) > 1){
		 return array("stock"=>false,"log"=>"No tracking variation.");
		 }
		 
		 foreach ($exp['offers'] as $offer){
		 
		 $price_offer = $offer['pricesInfo']['priceMap']['CURRENT']['price'];
		 
		 if($price_offer == $apiPrice){
		 if($offer['productAvailability']['availabilityStatus']=="OUT_OF_STOCK"){
		 $stock_status = false; ///OUT_OF_STOCK
		 }
		 if($offer['productAvailability']['availabilityStatus']=="IN_STOCK"){
		 $stock_status = true; ///IN_STOCK
		 }
		 break;
		 }
		 
		 }
		 }
		 
		 return array("stock"=>$stock_status, "log"=>"Json parsing is ok.");
		 */
	}
	
	public function checkStockStatusAgain($id, $apiPrice){
		
		return $this->checkStockStatusNewMethod($id, $apiPrice);
		
		/*
		
		return array("stock"=>false, "log"=>"HTML FROM PYTHON IS EMPTY.");
		
		$this->error = "";
		$url = "https://walmart.com/ip/".$id;
		
		$content = getContentWithProxyPython($url);
		
		if (! empty($content)) {
		
		$doc = new DOMDocument();
		$doc->validateOnParse = true;
		$doc->loadHTML('<meta http-equiv="content-type" content="text/html; charset=utf-8">' . $content);
		
		$shipMethod = strpos($content, '"shipMethod":"STANDARD"');
		$shipMethod2 = strpos($content, '"shipMethod":"VALUE"');
		$shipMethod3 = strpos($content, '"shipMethod":"FREIGHT"');
		
		if($shipMethod == true || $shipMethod2 == true || $shipMethod3 == true){
		
		if(strpos($content,"https://schema.org/OutOfStock") !== false){
		return array("stock"=>false,"log"=>"HTML is not empty.");
		}
		
		foreach($doc->getElementsByTagName('span') as $span){
		if($span->getAttribute('itemprop')=="price"){
		$price = $span->getAttribute('content');
		break;
		}
		}
		
		if($price != $apiPrice){
		return array("stock"=>false,"log"=>"HTML is not empty.");
		}
		
		return array("stock"=>true,"log"=>"HTML is not empty.");
		} else {
		return array("stock"=>false,"log"=>"HTML is not empty.");
		}
		
		} else {
		return array("stock"=>false,"log"=>"HTML FROM PYTHON IS EMPTY.");
		}
		
		return array("stock"=>false,"log"=>"HTML FROM PYTHON IS EMPTY.");
		*/
	}
	
	private function walmart_query_json($itemId) {
		
		$this->error = "";
		
		$request_url = "http://api.walmartlabs.com/v1/items/{$itemId}?format=json&apiKey=" . $this->apikey;
		$request_url = trim($request_url);
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $request_url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($ch);
		
		if (curl_errno($ch)) {
			$error = "HTTP error: " . curl_error($ch);
			curl_close($ch);
		} else {
			
			$json = json_decode($response);
			
			if ($json === false) {
				$le = error_get_last();
				$error = "Response could not be parsed. Try again later. Error information: " . $le['message'];
			} else {
				$walmart_error_1 = isset($json->errors) ? $json->errors[0]->message : "";
				
				if (empty($walmart_error_1)) {
					if (! isset($json->itemId)) {
						$curldata = curl_getinfo($ch);
						//var_dump($curldata);
						if ($curldata['http_code'] != 200) {
							$this->error = "non-200 HTTP error code : " . $curldata['http_code'];
							curl_close($ch);
							return false;
						} else {
							//var_dump($curldata);
							$this->error = "other HTTP error";
							curl_close($ch);
							return false;
						}
					}
					
					curl_close($ch);
					return $json;
				} else {
					$error = $walmart_error_1;
				}
			}
			
		}
		
		curl_close($ch);
		$this->error = $error;
		return false;
	}
	
	public function setError($error){
		$this->error = $error;
	}
	
	public function getError() {
		return $this->error;
	}
	
	public function getProduct($id) {
		
		$childJson = $this->walmart_query_json($id);
		
		if ($childJson === false) {
			return;
		}
		
		if (! isset($childJson->itemId)) {
			print_r($childJson);
			$this->error .= " unknown, json: " . $childJson;
			return;
		}
		
		$out = array();
		$extra = array();
		
		if(isset($childJson->sellerInfo)){
			$out['seller'] = $childJson->sellerInfo;
		}else{
			$out['seller'] = "Walmart";
		}
		
		$extra['seller'] = $out['seller'];
		
		$out['more_details'] = array();
		if(!isset($childJson->upc)){
			$out['upc'] = '';
		}else{
			$out['upc'] = $childJson->upc;
		}
		$out['id'] = $childJson->itemId;
		$out['price'] = isset($childJson->salePrice) ? $childJson->salePrice : 0;
		
		if($out['price']==0){
		    return;
		}
		
		$out['url'] = $childJson->productUrl;
		$out['currency'] = "USD";
		
		$thumbnailImage = str_replace("?odnHeight=450&odnWidth=450","?odnHeight=2000&odnWidth=2000",isset($childJson->thumbnailImage) ? $childJson->thumbnailImage : "");
		$out['picture'] = $thumbnailImage;
		
		$largeImage = str_replace("?odnHeight=450&odnWidth=450","?odnHeight=2000&odnWidth=2000",isset($childJson->largeImage) ? $childJson->largeImage : "");
		$out['largeImage'] = $largeImage;
		
		$out['title'] = $childJson->name;
		$out['inStock'] = isset($childJson->stock) ? ($childJson->stock == "Available" ? true : false) : false;
		
		if(isset($childJson->variants) && !empty($childJson->variants)){
			//twa e wariaciq
		}
		
		
		if($out['inStock'] == false){
			
			$getPriceAndStock = $this->getPriceAndStock($out['id']);
			
			if(!isset($getPriceAndStock['price'])){
				$this->error = "Cant catch price with proxy.";
				return;
			}
			
			$out['price'] = $getPriceAndStock['price'];
			$out['inStock'] = $getPriceAndStock['stock'];
			
		}
			 
		 if(empty($out['inStock'])){
			 $out['inStock'] = false;
		 }
		 
		
		if (isset($childJson->standardShipRate)) $extra['standardShipRate'] = $childJson->standardShipRate;
		/*if(!isset($childJson->standardShipRate)){
			if($out['price'] <= 50){
				$extra['standardShipRate'] = 6;
			} else {
				$extra['standardShipRate'] = 0;
			}
		}*/
		
		if(isset($childJson->freight) && $childJson->freight == TRUE){
			
			//$getFreightWithProx = $this->getFreightWithProxy($id);
			
			//$extra['standardShipRate'] = $getFreightWithProx['price'];
			//$out['inStock'] = $getFreightWithProx['stock'];
			
		}
		
		if (isset($childJson->twoThreeDayShippingRate)) $extra['twoThreeDayShippingRate'] = $childJson->twoThreeDayShippingRate;
		if (isset($childJson->overnightShippingRate)) $extra['overnightShippingRate'] = $childJson->overnightShippingRate;
		//if (isset($childJson->shortDescription)) $extra['description'] = $childJson->shortDescription;
		
		if (isset($childJson->attributes)) {
			foreach ($childJson->attributes as $attr => $val) {
				$extra[$attr] = $val;
			}
		}
		
		if(isset($childJson->freight) && $childJson->freight == TRUE){
			$extra['freight'] = true;
		}
		
		if(isset($childJson->finish) && $childJson->finish == TRUE){
			$extra['finish'] = $childJson->finish;
			//$extra['variation'] = "Finish:".$childJson->finish;
		}
		
		if(isset($childJson->size) && $childJson->size == TRUE){
			$extra['size'] = $childJson->size;
			//$extra['variation'] = "Size:".$childJson->size;
		}
		
		if(isset($childJson->color) && $childJson->color == TRUE){
			$extra['color'] = $childJson->color;
			//$extra['variation'] = "Color:".$childJson->color;
		}
		
		$out['extra_data'] = $extra;
		
		/*
		//product description
		if(isset($childJson->longDescription) && !empty($childJson->longDescription)){
			$out['more_details']['description'] = html_entity_decode($childJson->longDescription);
		}
		
		//product description
		if(isset($childJson->shortDescription) && !empty($childJson->shortDescription)){
			$out['more_details']['description'] = $childJson->shortDescription;
		}
		*/
		
		//product brand
		if(isset($childJson->brandName) && !empty($childJson->brandName)){
			$out['more_details']['brand'] = $childJson->brandName;
		}
		
		//product images
		if (isset($childJson->imageEntities)) {
			$i_img_g = 0;
			foreach ($childJson->imageEntities as $imgGall) {
				$largeImage = str_replace("?odnHeight=450&odnWidth=450", "?odnHeight=2000&odnWidth=2000", $imgGall->largeImage);
				$out['more_details']['image_gallery'][$i_img_g]['imageURL'] = $largeImage;
				$i_img_g ++;
			}
		}
		
		//DOM ELEMENTS LOADING
		$content = @file_get_contents("https://www.walmart.com/ip/".$id);
		
		if(empty($content)){
			return;
		}
		
		$dom = new DOMDocument();
		$dom->loadHTML($content);
		
		$features = array();
		$specifications = array();
		
		$json = $this->getJosnFromProductPage($content);
		$primary_id = $json['product']['selected']['product'];
		
		if(isset($json['product']['idmlMap'][$primary_id]['modules']['Specifications']['specifications']['values'])){
			$specifications_array = $json['product']['idmlMap'][$primary_id]['modules']['Specifications']['specifications']['values'];
			foreach($specifications_array[0] as $specific){
				foreach($specific as $spec){
					$specifications[$spec['displayName']] = $spec['displayValue'];
				}
			}
		}
		
		if(isset($json['product']['products'][$primary_id]['productAttributes']['detailedDescription'])){
			$detailed_description = $json['product']['products'][$primary_id]['productAttributes']['detailedDescription'];
			$dom->loadHTML($detailed_description);
			foreach($dom->getElementsByTagName("ul") as $ul){
				foreach($ul->getElementsByTagName("li") as $li){
					$features[] = $li->nodeValue;
				}
			}
		}
		
		if(isset($json['product']['idmlMap'][$primary_id]['modules']['ShortDescription']['product_short_description']['values'][0])){
			$out['more_details']['description'] = $json['product']['idmlMap'][$primary_id]['modules']['ShortDescription']['product_short_description']['values'][0];
		}
		
		foreach ($features as $i_f=>$feature){
			$out['more_details']['features'][$i_f]['future'] = $feature;
		}
		
		foreach ($specifications as $kAttr=>$specifics){
			$out['more_details']['specifics'][$kAttr] = $specifics;
		}
		
		return $out;
	}
	
	
	private function getImageInBigSize($image){
		
		$image = str_replace("?odnHeight=100&odnWidth=100","?odnHeight=2000&odnWidth=2000",$image);
		$image = str_replace("?odnHeight=450&odnWidth=450","?odnHeight=2000&odnWidth=2000",$image);
		
		return $image;
	}
	
	private function getJosnFromProductPage($content){
		
		$exp = explode("window.__WML_REDUX_INITIAL_STATE__ = {",$content);
		
		if(!isset($exp[1])){
			$this->error = "Cant catch json from page.";
			return;
		}
		
		$exp = explode("};};</script>",$exp[1]);
		if(!isset($exp[0])){
			$this->error = "Cant catch json from page. Error: #2";
			return;
		}
		
		$json = "{".$exp[0]."}";
		
		$json_array = json_decode($json,true);
		
		if(isset($json_array['product']['offers']) && empty($json_array['product']['offers'])){
			$doc = new DOMDocument();
			$doc->loadHTML($content);
			foreach($doc->getElementsByTagName("script") as $script){
				if($script->getAttribute("id")=="btf-content"){
					$json_array = $this->get_html_from_node($script);
					$json_array = json_decode($json_array,TRUE);
					$json_array = $json_array['btf-content'];
					break;
				}
			}
		}
		
		return $json_array;
		
	}
	
	private function get_html_from_node($node)
	{
		$html = '';
		$children = $node->childNodes;
		
		foreach ($children as $child) {
			$tmp_doc = new DOMDocument();
			$tmp_doc->appendChild($tmp_doc->importNode($child,true));
			$html .= $tmp_doc->saveHTML();
		}
		return $html;
	}
	
	public function getVariationsWithProxy($productId){
		
		$content = $this->getContentWithProxy($productId);
		
		/*if($productId == "26780115"){
		if($productId == "47215826"){
			var_dump($content);
		}*/
		
		if(isset($content['error'])){
			$this->error = "Cant catch content from page. Error with proxy.";
			return;
		}
		
		if(empty($content)){
			$this->error = "Cant catch content from page.";
			return;
		}
		
		$json_array = $this->getJosnFromProductPage($content);var_dump($json_array);die;
		
		if(empty($json_array)){
			$this->error = "Cant decode json.";
		}
		
		$priceOffers = array();
		
		if(!isset($json_array['product']['offers'])){
			$this->error = "Cant get product offers.";
			return;
		}
		
		$productInfo = array();
		
		if(isset($json_array['product']['midasContext'])){
			
			$context = $json_array['product']['midasContext'];
			
			$productInfo['title'] = $context['query'];
			$productInfo['brand'] = $context['brand'];
			$productInfo['manufacturer'] = $context['manufacturer'];
			$productInfo['free_shipping'] = $context['freeShipping'];
		}
		
		$imagesRank = array();
		$productImages = array();
		
		if(isset($json_array['product']['images'])){
			foreach ($json_array['product']['images'] as $offerId=>$image){
				
				if(empty($image['assetSizeUrls']['main'])){
					continue;
				}
				
				$image_url = $image['assetSizeUrls']['main'];
				
				if(!empty($image['assetSizeUrls']['zoom'])){
					$image_url = $image['assetSizeUrls']['zoom'];
				}
				$imagesRank[$image['rank']] = $this->getImageInBigSize($image_url);
				$productImages[] = $this->getImageInBigSize($image_url);
			}
		}
		
		$productVariants = array();
		
		if(isset($json_array['product']['variantCategoriesMap'])){
			foreach($json_array['product']['variantCategoriesMap'] as $variant){
				if(isset($variant['size'])){
					foreach($variant['size']['variants'] as $size_variants){
						foreach($size_variants['products'] as $s_v_product){//size variant products
							$productVariants[$s_v_product]['size']['name'] = $size_variants['name'];
							$productVariants[$s_v_product]['size']['cat'] = $variant['size']['name'];
						}
					}
				}
				if(isset($variant['actual_color'])){
					foreach($variant['actual_color']['variants'] as $actual_color_variants){
						foreach($actual_color_variants['products'] as $s_v_product){//size variant products
							$productVariants[$s_v_product]['actual_color']['name'] = $actual_color_variants['name'];
							$productVariants[$s_v_product]['actual_color']['cat'] = $variant['actual_color']['name'];
						}
					}
				}
				if(isset($variant['shoe_size'])){
					foreach($variant['shoe_size']['variants'] as $shoe_size_variants){
						foreach($shoe_size_variants['products'] as $s_v_product){//size variant products
							$productVariants[$s_v_product]['shoe_size']['name'] = $shoe_size_variants['name'];
							$productVariants[$s_v_product]['shoe_size']['cat'] = $variant['shoe_size']['name'];
						}
					}
				}
			}
		}
		
		if($productId=="26780115"){
			//var_dump($json_array);
		}
		
		foreach($json_array['product']['offers'] as $offerId=>$offer){
			
			$priceCurrency = "USD";
			$priceOffer = 0.00;
			$stockOffer = false;
			
			
			if($offer['productAvailability']['availabilityStatus']=="IN_STOCK"){
				$stockOffer = true;
			}
			
			if(isset($offer['fulfillment']['shippable'])){
				if($offer['fulfillment']['shippable']==false){
					$stockOffer = false;
				}
			}
			
			if(isset($offer['pricesInfo']['priceMap']['CURRENT'])){
				$priceInfo = $offer['pricesInfo']['priceMap']['CURRENT'];
				$priceOffer = $priceInfo['price'];
				$priceCurrency = $priceInfo['currencyUnit'];
			}
			
			$priceOffers[$offerId]['offer_id'] = $offerId;
			$priceOffers[$offerId]['offer_price'] = $priceOffer;
			$priceOffers[$offerId]['offer_currency'] = $priceCurrency;
			$priceOffers[$offerId]['stock_status'] = $stockOffer;
			
		}
		
		$products = array();
		
		foreach($json_array['product']['products'] as $product){
			
			$upc = '';
			$item_id = $product['usItemId'];
			$product_id = $product['productId'];
			
			if(isset($product['offers'][0])){
				$product_offer = $product['offers'][0];
			}else{
				$product_offer = array();
			}
			
			if(isset($product['upc'])){
				$upc = $product['upc'];
			}
			
			$products[$item_id]['product_offer'] = $product_offer;
			$products[$item_id]['product_upc'] = $upc;
			$products[$item_id]['product_id'] = $product_id;
			$products[$item_id]['product_item_id'] = $item_id;
		}
		
		$readyProducts = array();
		
		foreach($products as $item_id=>$product){
			$readyProducts[$item_id] = $product;
			if(isset($priceOffers[$product['product_offer']])){
				$readyProducts[$item_id]['offer'] = $priceOffers[$product['product_offer']];
			}else {
				$readyProducts[$item_id]['offer'] = array(); 
			}
		}
		
		$out = array();
		$i=0;
		foreach($readyProducts as $product){
			
			$out[$i]['id'] = $product['product_item_id'];
			$out[$i]['name'] = $productInfo['title'];
			$out[$i]['inStock'] = $product['offer']['stock_status'];;
			$out[$i]['productPrice'] = $product['offer']['offer_price'];
			$out[$i]['currency'] = $product['offer']['offer_currency'];
			
			//product images
			if (isset($productImages[1])) {
				$i_img_g = 0;
				foreach ($productImages as $largeImage) {
					$out[$i]['more_details']['image_gallery'][$i_img_g]['imageURL'] = $largeImage;
					$i_img_g ++;
				}
			}
			
			if(!isset($productImages[0])){
				$largeImage = NoImageURL();
			}else{
				$largeImage = $productImages[0]; //$productImages[array_rand($productImages)];
			}
			
			$out[$i]['thumbnailImage'] = $largeImage;
			$out[$i]['largeImage'] = $largeImage;
			
			if(isset($productVariants[$product['product_id']])){
				
				$productVariantsSingle = $productVariants[$product['product_id']];
				
				$out[$i]['variant_all'] = $productVariantsSingle;
				
				$out[$i]['variant'] = "";
				$out[$i]['variant_val'] = "";
				$out[$i]['variant_key'] = "";
				
				
				if(isset($productVariantsSingle['size'])){
					
					$attr = $productVariantsSingle['size']['cat'];
					$val = $productVariantsSingle['size']['name'];
					
					$out[$i]['variant'] .= ucfirst($attr) . ": " . $val . "<br />";
					$out[$i]['variant_key'] .= $attr;
					$out[$i]['variant_val'] .= $val;
					
				}
				
				if(isset($productVariantsSingle['shoe_size'])){
					
					$attr = $productVariantsSingle['shoe_size']['cat'];
					$val = $productVariantsSingle['shoe_size']['name'];
					
					$out[$i]['variant'] .= ucfirst($attr) . ": " . $val . "<br />";
					$out[$i]['variant_key'] .= $attr;
					$out[$i]['variant_val'] .= $val;
					
				}
				
				
				if(isset($productVariantsSingle['actual_color'])){
					
					$attr = $productVariantsSingle['actual_color']['cat'];
					$val = $productVariantsSingle['actual_color']['name'];
					
					$out[$i]['variant'] .= ucfirst($attr) . ": " . $val . "<br />";
					$out[$i]['variant_key'] .= $attr;
					$out[$i]['variant_val'] .= $val;
					
				}
			}
			
			if($out[$i]['productPrice'] <= 35){
				$out[$i]['standardShipRate'] = 6;
			} else {
				$out[$i]['standardShipRate'] = 0;
			}
			
			$i++;
		}
		
		return $out;
	}
	
	private function getContentWithProxy($productId){
		
		$content = "";
		
		$url = "https://www.walmart.com/ip/".$productId;
		
		if($this->work_with_proxy == TRUE){
		   
			$this->ci->load->helper("ProxyCURL");
			$this->ci->load->model("Proxy_model", "proxy", TRUE);
			
			$proxyData = $this->ci->proxy->getData();
			
			if(empty($proxyData)){
				$this->setError("Cant get proxy data.");
				return;
			}
			
			$back_data = getDataProxyCURL($proxyData["ip"], $proxyData["port"], $proxyData["username"], $proxyData["password"], $url);
			
			if ($back_data["error"] == true) {
				$this->setError("Proxy error: ".$back_data["error"]);
				$this->ci->proxy->markErrored($proxy["id"]);
			} else {
				$content = $back_data["result"];
			}
			
		} else {
			$content = file_get_contents($url);
		}
		
		return $content;
		
	}
	
	private function getPriceWithProxy($productId){
		
		$doc = new DOMDocument();
		$doc->validateOnParse = true;
		$doc->loadHTML($this->getContentWithProxy($productId));
		
		//select elements by DOM X PATH
		$Xpath = new DOMXpath($doc);
		$price = 0;
		foreach($doc->getElementsByTagName('span') as $div){
			if(strpos($div->getAttribute("itemprop"),"price")!==false){
				$price = $div->getAttribute("content");
			}
		}
		
		return $price;
		
	}
	
	private function getFreightWithProxy($productId){
		
		$price = 0;
		$old_price = 0;
		
		$ci = & get_instance();
		$rowProduct = $ci->db->query("SELECT * FROM products WHERE remote_id='{$productId}'")->row_array();
		$extra_data = json_decode($rowProduct['extra_data'], TRUE);
		
		if(isset($extra_data)){
			$old_price = $extra_data['standardShipRate'];
		}
		
		$stock = false;
		$retry = false;
		
		$content = $this->getContentWithProxy($productId);
		
		if(!empty($content)){
			
			preg_match('/("shippingPrice":)\d+((,\d+)+)?(.\d+)?(.\d+)?(,\d+)?/',$content,$match);
			
			if(!isset($match[0])){
				$stock = false;
			}
			
			if(isset($match[0])){
				
				$price = $match[0];
				$price = str_replace('"shippingPrice":',"",$price);
				
				if(empty($price)){
					$retry = true;
					$price = $old_price;
					if($price==0){
						$stock = false;
					}
				}
				
				if(!empty($price)){
					$stock = true;
				}
			}
			
		}
		
		if(empty($content)){
			$stock = false;
		}
		
		/*
		 if($retry == true){
		 for ($i = 1; $i <= 3; $i++) {
		 }
		 }
		 */
		
		return array("price"=>$price, "stock"=>$stock);
	}
	
	public function getVariations($id) {
		
		if (! empty($this->error)) {
			return array("error" => $this->error);
		} else {
			$json = $this->walmart_query_json($id);
			$json = json_decode(json_encode($json), TRUE);
			
			
			if (! empty($json)) {
				$out = array();
				$i = 0;
				
				if (! isset($json['variants'])) {
					$json['variants'] = array($id);
				}
				
				$allVariantIds = [];
				foreach($json['variants'] as $variant){
					$allVariantIds[$variant] = true;
				}
				
				if(!isset($allVariantIds[$id])){
					$nextKeyVariation = sizeof($json['variants']) + 1;
					$json['variants'][$nextKeyVariation] = $id;
				}
				
				foreach ($json['variants'] as $variant) {
					
					if ($variant == $id) {
						$childJson = $json;
					} else {
					}
					
					$childJson = $this->walmart_query_json($variant);
					
					$price = $childJson->salePrice;
					
					/*
					 if($childJson->salePrice > 150){
					 $priceProxy = $this->getPriceWithProxy($childJson->itemId);
					 if($priceProxy > $childJson->salePrice){
					 $price = $childJson->salePrice;
					 }
					 }
					 */
					
					$out[$i]['id'] = $childJson->itemId;
					$out[$i]['productPrice'] = $price;
					$out[$i]['currency'] = "USD";
					
					if (isset($childJson->standardShipRate)) $out[$i]['standardShipRate'] = $childJson->standardShipRate;
					if(!isset($childJson->standardShipRate)){
						if($out['price'] <= 50){
							$out[$i]['standardShipRate'] = 6;
						} else {
							$out[$i]['standardShipRate'] = 0;
						}
					}
					
					$out[$i]['standardShipRate'] = $childJson->standardShipRate;
					$out[$i]['twoThreeDayShippingRate'] = isset($childJson->twoThreeDayShippingRate) ? $childJson->twoThreeDayShippingRate : - 1;
					$out[$i]['overnightShippingRate'] = isset($childJson->overnightShippingRate) ? $childJson->overnightShippingRate : - 1;
					
					$thumbnailImage = str_replace("?odnHeight=100&odnWidth=100","?odnHeight=2000&odnWidth=2000",$childJson->thumbnailImage);
					$thumbnailImage = str_replace("?odnHeight=450&odnWidth=450","?odnHeight=2000&odnWidth=2000",$thumbnailImage);
					
					$out[$i]['thumbnailImage'] = $thumbnailImage;
					
					$largeImage = str_replace("?odnHeight=100&odnWidth=100","?odnHeight=2000&odnWidth=2000",$childJson->largeImage);
					$largeImage = str_replace("?odnHeight=450&odnWidth=450","?odnHeight=2000&odnWidth=2000",$largeImage);
					
					$out[$i]['largeImage'] = $largeImage;
					$out[$i]['description'] = isset($childJson->shortDescription) ? $childJson->shortDescription : "";
					$out[$i]['name'] = $childJson->name;
					$out[$i]['inStock'] = $childJson->stock == "Available" ? true : false;
					
					if(isset($childJson->freight) && $childJson->freight == TRUE){
						
						//$getFreightWithProxy = $this->getFreightWithProxy($id);
						
						//$out[$i]['standardShipRate'] = $getFreightWithProxy['price'];
						//$out[$i]['inStock'] = $getFreightWithProxy['stock'];
					}
					
					if(isset($childJson->variants) && !empty($childJson->variants)){
						//variation
					}
					
					
					 if($out[$i]['inStock'] == false){
					 	
						 $getPriceAndStock = $this->getPriceAndStock($out[$i]['id']);
						 
						 $out[$i]['productPrice'] = $getPriceAndStock['price'];
						 $out[$i]['inStock'] = $getPriceAndStock['stock'];
						 
					 }
					
					if (isset($childJson->attributes)) {
						
						$out[$i]['variant'] = "";
						$out[$i]['variant_val'] = "";
						$out[$i]['variant_key'] = "";
						
						foreach ($childJson->attributes as $attr => $val) {
							//$out[$i]['variant'] .= ucfirst($attr) . ": " . $val . "<br />";
							$out[$i]['variant_key'] .= $attr;
							$out[$i]['variant_val'] .= $val;
						}
					}
					
					if(isset($childJson->size)){
						$attr = "Size";
						$val = $childJson->size;
						$out[$i]['variant'] .= " ".ucfirst($attr) . ": " . $val . "<br />";
						$out[$i]['variant_key'] .= $attr;
						$out[$i]['variant_val'] .= $val;
					}
					
					if(isset($childJson->color)){
						$attr = "Color";
						$val = $childJson->color;
						$out[$i]['variant'] .= " ".ucfirst($attr) . ": " . $val . "<br />";
						$out[$i]['variant_key'] .= $attr;
						$out[$i]['variant_val'] .= $val;
					}
					
					$i++;
				}
			} else {
				$out = null;
			}
			
			return $out;
		}
	}
	
	public function searchProducts($params) {
		
		$items = array();
		$categoryId = $params['category'];
		$min_price = $params['min_price'];
		$max_price = $params['max_price'];
		$keyword = $params['keyword'];
		$per_page = $params['per_page'];
		$start_page = $params['start_page'];
		$sort_by = $params['sort_by'];
		$order_by = $params['order_by'];
		
		if (! empty($categoryId)) {
			$searchParams = "&categoryId=" . $categoryId;
		} else {
			$searchParams = '';
		}
		if ($sort_by == "price") {
			$searchParams .= '&sort=price&order=' . $order_by;
		} elseif ($sort_by == "high_rating") {
			$searchParams .= '&sort=customerRating&order=desc';
		}
		// echo $sort_by;
		if ($min_price != NULL) {
			if ($max_price == NULL) {
				$max_price = 10000;
			}
			if ($min_price != NULL && $max_price != NULL) {
				$searchParams .= '&facet=on&facet.range=price:[' . $min_price . '+TO+' . $max_price . ']';
			}
		} else {
			if ($min_price == NULL) {
				$min_price = 0;
			}
			if ($max_price != NULL) {
				$searchParams .= '&facet=on&facet.range=price:[' . $min_price . '+TO+' . $max_price . ']';
			}
		}
		$keyword = urlencode($keyword);
		$request_url = "http://api.walmartlabs.com/v1/search?apiKey=" . $this->apikey . '&query=' . $keyword . $searchParams . '&numItems=' . $per_page . '&start=' . $start_page;
		$response = @file_get_contents($request_url);
		// echo $request_url;
		if ($response === false) {
			$data['totalResults'] = null;
			$data['items'] = null;
			$error = "Request failed. Try again later.";
		} else {
			$json = json_decode($response);
			if ($json->totalResults > 0) {
				$i = 0;
				foreach ($json->items as $item) {
					$items[$i]['itemID'] = $item->itemId;
					$items[$i]['itemName'] = $item->name;
					$items[$i]['salePrice'] = $item->salePrice;
					$items[$i]['upc'] = isset($item->upc) ? $item->upc : 'No upc.';
					$items[$i]['itemImage'] = $item->thumbnailImage;
					$items[$i]['itemRating'] = isset($item->customerRatingImage) ? '<img src="' . $item->customerRatingImage . '" />' : "No rating";
					// $items[$i]['itemStock'] = isset($item->stock) ? true : false;
					$items[$i]['itemStock'] = true;
					$items[$i]['itemUrl'] = $item->productUrl;
					
					$i++;
				}
				$data['totalResults'] = $json->totalResults;
				$data['items'] = $items;
			} else {
				$data['totalResults'] = null;
				$data['items'] = null;
			}
		}
		return $data;
	}
	
	public function getCategories() {
		$data = array();
		
		$request_url = "http://api.walmartlabs.com/v1/taxonomy?apiKey=" . $this->apikey;
		$response = @file_get_contents($request_url);
		
		if ($response === false) {
			$error = "Request failed. Try again later.";
		} else {
			$json = json_decode($response);
			$i = 0;
			foreach ($json->categories as $cat) {
				$data[$cat->id] = $cat->name;
				$i++;
			}
		}
		return $data;
	}
	
	public function parseId($idOrURL) {
		if (strlen($idOrURL) > 10) {
			$last = explode("/", $idOrURL);
			$last = end($last);
			$id = explode("?", $last);
			
			$numid = preg_replace("/[^0-9,.]/", "", $id[0]);
			
			return $numid;
		}
		
		return $idOrURL;
	}
	
}
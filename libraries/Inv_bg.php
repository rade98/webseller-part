<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Inv_bg
{
	private $username = "name@webseller.guru";
      private $password = "test";
	private $baseURL = "https://guru.inv.bg/RESTapi/";

	public function __construct()
	{
		require_once APPPATH . 'third_party/vendor/autoload.php';
	}

	public function getLists($type)
	{
		$response = \Httpful\Request::get($this->baseURL. $type)
			        ->authenticateWith($this->username, $this->password)
			        ->sendsJson()
	                ->expectsType("json")
			        ->send();
        
		return json_decode($response, true);
	}
	public function getInvoices()
	{
		$today_date = date("Y-m-d");
		$last7 = date("Y-n-j", strtotime("first day of previous month"));
		$invoices = $this->getLists("invoices?from_date=2018-03-01&to_date=".$today_date);
		return $invoices['invoices'];
	}
	public function invoiceDetails($invoice)
	{
	    $response = \Httpful\Request::get($this->baseURL."invoice/pdf/".$invoice)
		            ->authenticateWith($this->username, $this->password)
		            ->sendsType(\Httpful\Mime::FORM)
		            ->body($invoice)
		            ->sendsJson()
		            ->expectsType("json")
		            ->send();

	    return ($response->body);
	}
	public function deleteInvoice($id)
	{	     	
		$response = \Httpful\Request::delete($this->baseURL."invoice/".$id)
		     	     ->authenticateWith($this->username, $this->password)
		     	     ->sendsJson()
		     	     ->expectsType("json")
		     	     ->send();

		return json_decode($response, true);
	} 
	public function changeStatus($id)
	{	     	
		$response = \Httpful\Request::put($this->baseURL."invoice/".$id)
		     	     ->authenticateWith($this->username, $this->password)
		     	     ->sendsJson()
		     	     ->status("paid")
		     	     ->expectsType("json")
		     	     ->send();

		return json_decode($response, true);
	}  
	public function getClients()
	{
		$clients = $this->getLists("clients");
		return $clients['clients'];
	}
	public function addClient()
	{
	    $response = \Httpful\Request::post($this->baseURL."client")
	                ->authenticateWith($this->username, $this->password)
		            ->sendsType(\Httpful\Mime::FORM)
		            ->body($client_data)
		            ->sendsJson()
		            ->expectsType("json")
		            ->send();
		   
		return json_decode($response, true);
	}
	public function addInvoice($invoice_data)
	{
	    $response = \Httpful\Request::post($this->baseURL."invoice")
	                ->authenticateWith($this->username, $this->password)
		            ->sendsType(\Httpful\Mime::FORM)
		            ->body($invoice_data)
		            ->sendsJson()
		            ->expectsType("json")
		            ->send();
		   
		return json_decode($response, true);
	}
	public function sendEmail($mail_data)
	{
	    $response = \Httpful\Request::post($this->baseURL."invoice/send/".$mail_data['id'])
	                ->authenticateWith($this->username, $this->password)
		            ->sendsType(\Httpful\Mime::FORM)
		            ->body($mail_data)
		            ->sendsJson()
		            ->expectsType("json")
		            ->send();
		   
		return json_decode($response, true);
	}
	public function checkClientifExists()
	{
	    $clients = $this->getLists("clients");
	}

	public function chechInvoicesifExists($id)
	{
         $this->roles_model->checkInvoice($id);
	}
     function checkInvoice()
    {
   $search_array = array($this->getInvoices());
if (array_key_exists('id', $search_array)) {
    echo "ELEMENT FIRST EXISTS!";
    }else{
	echo "Koj kurac";
	var_dump($search_array);
  }
}
}
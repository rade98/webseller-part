<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* API DOCUMENTATION: https://github.com/robwittman/shopify-php-sdk
*/

class Shopify_handler {

	private $API_KEY;
	private $API_SECRET_KEY;
	private $REFRESH_TOKEN;
	private $STORE;
	private $DOMAIN_SERVER;

	public function __construct($config) {
		// Include the google api php libraries
		require_once 'application/third_party/vendor/autoload.php';
		$this->setConfig($config);
	}

	public function index(){
		echo 'It works!';
	}

	public function test(){

		$client = new Shopify\Api(array(
		    'api_key' => $this->API_KEY,
		    'api_secret' => $this->API_SECRET_KEY,
		    'myshopify_domain' => $this->STORE,
		    'access_token' => $this->REFRESH_TOKEN
		));

		$service = new Shopify\Service\ProductService($client);
		$products = $service->all();


		return $products;
	}
	private function setConfig($config){
		$this->API_KEY = $config['API_KEY'];
		$this->API_SECRET_KEY = $config['API_SECRET_KEY'];
		$this->REFRESH_TOKEN = $config['REFRESH_TOKEN'];
		$this->DOMAIN_SERVER = $config['DOMAIN_SERVER'];
		$this->STORE = $config['STORE'] .".". $this->DOMAIN_SERVER;
	}
}
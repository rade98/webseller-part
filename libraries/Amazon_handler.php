<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once ("interfaces/IStoreHandler.php");

class Amazon_handler implements IStoreHandler {

	private $ci;

	private $error = "";

	private $privkey;

	private $pubkey;

	private $asstag;

	private $store;

	private $availOOS = array();
	
	private $futureDate = array();

	private $regex = '~(?:\b)((?=[0-9a-z]*\d)[0-9a-z]{10})(?:\b)~i';

	private $last_response = array();
	
	private $work_with_proxy = false;
	
	public function __construct($data) {
		
		require_once 'vendor/rmccue/requests/library/Requests.php';
		
		Requests::register_autoloader();

		$this->ci =& get_instance();
		/*
		$this->ci->load->library("amazonprime_handler", array());
		*/
		if (isset($data['json'])) {
			$this->setApiKey($data['json']);
		}
		
		if (isset($data['store'])) {
			$this->store = $data['store'];
		}
		
		$this->availOOS = getAmazonAvailOOS();
		$this->futureDate = getAmazonFutureDate();
	}

	public function setApiKey($key) {
		
		$keyObj = json_decode($key);
		
		if (empty($keyObj->privkey)) {
			$this->error = "Something went wrong. Please, contact support.";
			return null;
		} else {
			$this->privkey = $keyObj->privkey;
			$this->pubkey = $keyObj->pubkey;
			$this->asstag = $keyObj->asstag;
			return true;
		}
	}

	public function setStore($store) {
		$this->store = $store;
	}
	
	public function getStore() {
		return $this->store;
	}
	
	private function getStoreUrl(){
		
		$url = $this->store['url'];
		$url = str_replace("webservices.", "", $url);
		
		if(strpos($url, "http://") !==false){
			
		}else{
			$url = "http://".$url;
		}
		
		return $url;
		
	}
	
	public function workWithProxy($work){
		$this->work_with_proxy = $work;
	}
	
	/*
	 * Parameters: $region - the Amazon(r) region (ca,com,co.uk,de,fr,co.jp) $params - an array of parameters, eg. array("Operation"=>"ItemLookup", "ItemId"=>"B000X9FLKM", "ResponseGroup"=>"Small") $public_key - your "Access Key ID" $private_key - your "Secret Access Key" $version (optional)
	 */
	private function aws_signed_request($params, $public_key, $private_key, $associate_tag = NULL, $version = '2011-08-01') {
		$method = 'GET';
		$host = $this->store['url'];
		$uri = '/onca/xml';
		
		$params['Service'] = 'AWSECommerceService';
		$params['AWSAccessKeyId'] = $public_key;
		$params['Timestamp'] = gmdate('Y-m-d\TH:i:s\Z');
		$params['Version'] = $version;
		if ($associate_tag !== NULL) {
			$params['AssociateTag'] = $associate_tag;
		}
		
		ksort($params);
		
		$canonicalized_query = array();
		foreach ($params as $param => $value) {
			$param = str_replace('%7E', '~', rawurlencode($param));
			$value = str_replace('%7E', '~', rawurlencode($value));
			$canonicalized_query[] = $param . '=' . $value;
		}
		$canonicalized_query = implode('&', $canonicalized_query);
		
		$string_to_sign = $method . "\n" . $host . "\n" . $uri . "\n" . $canonicalized_query;
		
		$signature = base64_encode(hash_hmac('sha256', $string_to_sign, $private_key, TRUE));
		$signature = str_replace('%7E', '~', rawurlencode($signature));
		
		$request = 'http://' . $host . $uri . '?' . $canonicalized_query . '&Signature=' . $signature;
		
		return $request;
	}

	private function amazon_query_xml($itemid, $respGroup) {
		$this->error = "";
		$request = $this->aws_signed_request(array(
			'Operation' => 'ItemLookup',
			'ItemId' => $itemid,
			'ResponseGroup' => $respGroup,
			'MerchantId' => 'All'
		), $this->pubkey, $this->privkey, $this->asstag);

		//echo $request;
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $request);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 75);
		curl_setopt($ch, CURLOPT_TIMEOUT, 75);
		
		$response = curl_exec($ch);
		
		if (! curl_errno($ch)) {
			curl_close($ch);
			$xml = simplexml_load_string($response);
			
			if ($xml === false) {
				$le = error_get_last();
				$this->error = "Response could not be parsed. Try again later. Error information: " . $le['message'];
				
				return false;
			}
			else {
				
				if (isset($xml->Error->Message)) {
					$this->error .= $xml->Error->Message;
				}
				
				if (isset($xml->Items->Request->Errors->Error)) {
					foreach ($xml->Items->Request->Errors->Error as $error) {
						$this->error .= "[;;ERR] " . $error->Message;
					}
				}
				
				if(strpos($this->error,"submitting requests too quickly")!==false){
					
					$retry = $this->amazon_query_xml($itemid, $respGroup);
					
					if($retry == false){
						$retry = $this->amazon_query_xml($itemid, $respGroup); 
					}
					
					return $retry;
				}
				
				return $xml;
			}
		}
		else {
			$this->error = "HTTP error: " . curl_error($ch);
			curl_close($ch);
		}
		
		return false;
	}

	public function getError() {
		return $this->error;
	}
	
	public function clearError(){
		$this->error = false;
	}
	
	public function autoSaveProductNEW(){
		
		$products = array();
		
		if(!empty($this->last_response)){
		
			foreach($this->last_response as $resp){
				
				if(!isset($resp['offers'][0]['price']['currency']) || empty($resp['offers'][0]['price']['currency'])){
					continue;
				}
				
				if(!isset($resp['offers'][0]['price']['cents']) || empty($resp['offers'][0]['price']['cents'])){
					continue;
				}
				
				$product['remote_id'] = $resp['asin'];
				$product['title'] = $resp['title'];
				$product['url'] = $resp['url'];
				$product['currency'] = $resp['offers'][0]['price']['currency'];
				$product['price'] = $resp['offers'][0]['price']['cents'] / 100;
				$product['picture'] = $resp['thumbnailImage'];
				$product['largeImage'] = $resp['largeImage'];
				$product['inStock'] = $resp['inStock'];
				$product['extra_data'] = json_decode($resp['extra_data']);
				$product['more_details'] = $resp['more_details'];
				
				$products[] = $product;
				
			}
			
		}
		
		return $products;
	}

	public function getProductWithoutAPI($id){
		
		$this->ci = & get_instance();
		$this->ci->load->library("Amazonprime_handler");
		
		$product = $this->ci->amazonprime_handler->getProduct($id);
		
		return $product;
		
	}
	
	public function getProductFromVariations($productId, $variations){
		
		foreach($variations as $product){
			
			if(isset($product['asin'])){
				if($product['asin'] == $productId){
					
					if(empty($product['title'])) return;
					if(empty($product['offers'][0]['price']['formatted'])) return;
					
					$price = $product['offers'][0]['price']['formatted'];
					$price = str_replace("$","",$price);
					
					$dataArray['more_details'] = $product['more_details'];
					$dataArray['inStock'] = $product['inStock'];
					$dataArray['id'] = $product['asin'];
					$dataArray['url'] = $this->getStoreUrl() . "/dp/".$product['asin'];
					$dataArray['sku'] = $product['asin'];
					$dataArray['currency'] = "USD";
					$dataArray['thumbnailImage'] = isset($product['thumbnailImage']) ? $product['thumbnailImage']:"";
					$dataArray['title'] = trim($product['title']);
					$dataArray['price'] = $price;
					$dataArray['picture'] = isset($product['thumbnailImage']) ? $product['thumbnailImage']:"";
					$dataArray['largeImage'] = isset($product['largeImage']) ? $product['largeImage']:"";
					$dataArray['extra_data'] = $product['extra_data'];
						
					return $dataArray;
						
					break;
					
				}
			}
			
		}
		
		return false;
	}
	
	public function getProduct($id) {
		
		if($this->work_with_proxy == true){
			$this->clearError();
			$return = $this->getProductWithProxy($id);
			return $return;
		}
		
		$xml = $this->amazon_query_xml($id, "Offers, Medium, Variations");
		
		/*
		if($id=="B0748NFF5L"){
			var_dump($xml);
			die();
		}
		*/
		
		$var = $xml->Items->Item;
		
		if(empty($var)){
			//$this->error = $xml->Error->Message;
			//return;
			$this->clearError();
			$return = $this->getProductWithProxy($id);
			return $return;
		}
		
		$out = array();
		
		if(isset($var->ParentASIN)){
			$out['parent_asin'] = $var->ParentASIN->__toString();
		}
		
		$out['recived_by_api'] = 1;
		$out['upc'] = $var->ItemAttributes->UPC->__toString();
		$out['remote_id'] = $var->ASIN->__toString();
		$out['title'] = $var->ItemAttributes->Title->__toString();
		$out['url'] = $var->DetailPageURL->__toString();
		$out['picture'] = isset($var->SmallImage->URL) ? $var->SmallImage->URL->__toString() : "";
		
		$out['more_details'] = array();

		if (isset($var->ItemAttributes->Feature)) {
			$i_f = 0;
			foreach ($var->ItemAttributes->Feature as $Feature) {
				$out['more_details']['features'][$i_f]['future'] = $Feature->__toString();
				$i_f ++;
			}
		}
		
		foreach(json_decode(json_encode($var->ItemAttributes), TRUE) as $kAttr=>$iAttr){
			if(is_string($iAttr)){
				$out['more_details']['specifics'][$kAttr] = $iAttr;
			}
			unset($out['more_details']['specifics']['Title']);
		}
		
		if (isset($var->ItemAttributes->Brand)) {
			$out['more_details']['brand'] = $var->ItemAttributes->Brand->__toString();
		}
		if (isset($var->ItemAttributes->EAN)) {
			$out['more_details']['ean'] = $var->ItemAttributes->EAN->__toString();
		}
		if (isset($var->ItemAttributes->Department)) {
			$out['more_details']['department'] = $var->ItemAttributes->Department->__toString();
		}
		
		if(isset($var->EditorialReviews->EditorialReview->Content)){
			$out['more_details']['description'] = $var->EditorialReviews->EditorialReview->Content->__toString();
		}
		
		$lowestOffer = - 1;
		$lowestOfferPrice = PHP_INT_MAX;
		
		$lowestPrimeOffer = - 1;
		$lowestPrimeOfferPrice = PHP_INT_MAX;
		
		$j = 0;
		if (isset($var->Offers->Offer)) {
			foreach ($var->Offers->Offer as $offer) {
				$out['offers'][$j]['stock'] = true;
				if (isset($offer->OfferListing->Availability)) {
					$avail = strtolower($offer->OfferListing->Availability);
					foreach ($this->availOOS as $testAvail) {
						if (strpos($avail, $testAvail) !== false) {
							$out['offers'][$j]['stock'] = false;
						}
					}
					foreach ($this->futureDate as $fdTxt) {
						if (strpos($avail, $fdTxt) !== false) {
							$extra['future_date'] = true;
						}
					}
					if($avail == "futuredate"){
						$extra['future_date'] = true;
					}
				}
				if(isset($offer->OfferListing->AvailabilityAttributes->AvailabilityType)){
						$avail_type = strtolower($offer->OfferListing->AvailabilityAttributes->AvailabilityType);
						foreach ($this->availOOS as $testAvail) {
							if (strpos($avail_type, $testAvail) !== false) {
								$out['offers'][$j]['stock'] = false;
							}
						}
						if($avail_type == "futuredate"){
							$extra['future_date'] = true;
						}
				}
				
				$out['offers'][$j]['prime'] = $offer->OfferListing->IsEligibleForPrime == 1 ? true : false;
				$priceKey = isset($offer->OfferListing->SalePrice) ? $offer->OfferListing->SalePrice : $offer->OfferListing->Price;
				
				$out['offers'][$j]['price']['formatted'] = $priceKey->FormattedPrice->__toString();
				$out['offers'][$j]['price']['cents'] = $priceKey->Amount->__toString();
				$out['offers'][$j]['price']['currency'] = $priceKey->CurrencyCode->__toString();
				
				if ($lowestOfferPrice > $priceKey->Amount) {
					$lowestOfferPrice = $priceKey->Amount;
					$lowestOffer = $j;
				}
				
				if ($out['offers'][$j]['prime'] && $lowestPrimeOfferPrice > $priceKey->Amount) {
					$lowestPrimeOfferPrice = $priceKey->Amount;
					$lowestPrimeOffer = $j;
				}
				
				$j ++;
			}
		}
		
		$variant = "";
		if (isset($var->VariationAttributes)) {
			foreach ($var->VariationAttributes->VariationAttribute as $attr) {
				$variant .= $attr->Name . ": " . $attr->Value . "<br />";
			}
		}
		
		$content = $this->getProductPageWithProxy($out['remote_id']);
		
		//$content = $this->getProductPageWithScrapServer($out['remote_id']); 
		
		/*
		$url_sup  = $this->getSupplierHost() . "/dp/".$out['remote_id'];
		
		$request = Requests::get($url_sup);
		$content = $request->body;
		*/
		
		
		$proxy_images = $this->getImagesWithProxy($content);
		$proxy_description = $this->getDescriptionWithProxy($content); 
		
		if(!empty($proxy_description)){
			$out['more_details']['description'] = $proxy_description;
		}
		 
		//$proxy_images = [];
		
		if(!empty($proxy_images)){
			
			$out['proxy_images'] = $proxy_images;
			
			if(isset($proxy_images['pictures'])){
				$out['more_details']['image_gallery'] = array();
				$i=0;
				foreach($proxy_images['pictures'] as $image){
					$out['more_details']['image_gallery'][$i]['imageURL'] = $image;
					$i++;
				}
			}
			
			if(isset($proxy_images['variants'])){
				$attribute = $var->ItemAttributes->Color->__toString();
				$out['more_details']['image_gallery'] = array();
				$i=0;
				foreach($proxy_images['variants'][$attribute] as $image){
					$out['more_details']['image_gallery'][$i]['imageURL'] = $image;
					$i++;
				}
			}
			if(isset($out['more_details']['image_gallery'][0]['imageURL'])){
				$out['picture'] = $out['more_details']['image_gallery'][0]['imageURL'];
				$out['largeImage'] = $out['more_details']['image_gallery'][0]['imageURL'];
			}
		} else {
			if (isset($var->ImageSets->ImageSet)) {
				$i_img_g = 0;
				foreach ($var->ImageSets->ImageSet as $imgGall) {
					$out['more_details']['image_gallery'][$i_img_g]['imageURL'] = $imgGall->LargeImage->URL->__toString();
					$i_img_g ++;
				}
			}
		}
		
		if(isset($out['more_details']['image_gallery'][0]['imageURL'])){
			$out['picture'] = $out['more_details']['image_gallery'][0]['imageURL'];
			$out['largeImage'] = $out['more_details']['image_gallery'][0]['imageURL'];
		}
		
		$out['more_details']['upc'] = $out['upc'];
		
		if ($lowestOffer == - 1) {
			//puskame proxy
			$this->clearError();
			$return = $this->getProductWithProxy($id);
			return $return;
		}
		
		$out['lowestPrimeOffer'] = $lowestPrimeOffer;
		$out['primePrice'] = $lowestPrimeOfferPrice;
		
		$out['lowestOffer'] = $lowestOffer;
		$out['inStock'] = $j == 0 ? false : $out['offers'][$lowestOffer]['stock'];
		
		$lowest = $out['offers'][$lowestOffer];
		
		$out['price'] = $lowest['price']['cents'] / 100;
		$out['currency'] = $lowest['price']['currency'];
		
		$extra['prime'] = $lowest['prime'];
		$out['largeImage'] = isset($var->LargeImage->URL) ? $var->LargeImage->URL->__toString() : "";
		
		if (! empty($variant)) {
			$extra['variant'] = $variant;
		}
		
		$out['extra_data'] = $extra; // json_encode($extra);
		
		
		return $out;
	}

	public function getParentASIN($asin) {
		$xml = $this->amazon_query_xml($asin, "ItemIds");
		return isset($xml->Items->Item->ParentASIN) ? $xml->Items->Item->ParentASIN : $asin;
	}


	public function getVariations($asin){

		$api_request = $this->getVariationsWithAPI($asin);
		
		if($api_request == false || isset($api_request[0]['inStock']) && $api_request[0]['inStock'] == false){
			
			$api_request = $this->getVariationsWithProxy($asin);
			
			if(empty($api_request)){
				$api_request = false;
			}
			if($api_request){
				$this->error = false;
			}
		}
	
		return $api_request;
	}

	public function getVariationsWithProxy($asin){
		
		$this->ci = & get_instance();
		$this->ci->load->library("Amazonprime_handler");
		$this->ci->amazonprime_handler->setStore($this->getStore());

		$variations = $this->ci->amazonprime_handler->getVariations($asin, array("all_offers"=>true));//true for all offers
		
		if(!empty($this->ci->amazonprime_handler->getError())){
			$this->error = $this->ci->amazonprime_handler->getError();
		}
		
		if(empty($variations)) return;
		
		return $variations;
	}
	
	public function setProductCatchedContentLinks($type, $link){
		
		$this->ci =& get_instance();
		
		$this->ci->load->library("Amazonprime_handler");
		
		$this->ci->amazonprime_handler->setProductCatchedContentLinks($type, $link);
		
	}
	
	public function setResponseSettings($response){
		
		$this->ci =& get_instance();
		$this->ci->load->library("Amazonprime_handler");
		
		$this->ci->amazonprime_handler->setResponseSettings($response);
		
	}
	
	public function getResponseSettings(){
		
		$this->ci =& get_instance();
		$this->ci->load->library("Amazonprime_handler");
		
		$this->ci->amazonprime_handler->getResponseSettings();
		
	}
	
	public function getProductContentLinks($id){
		
		$this->ci =& get_instance();
		$this->ci->load->library("Amazonprime_handler");
		$this->ci->amazonprime_handler->setStore($this->getStore());
		
		return $this->ci->amazonprime_handler->getProductContentLinks($id);
	}

	public function getProductWithProxy($id){
		
		$this->ci =& get_instance();
		$this->ci->load->library("Amazonprime_handler");
		
		$this->ci->amazonprime_handler->setStore($this->getStore()); 
		
		$product = $this->ci->amazonprime_handler->getProduct($id, array("all_offers"=>true));//true for all offers
		
		if(isset($product['price']) && $product['price'] > 0){
			$this->clearError();
		}
		
		if(!empty($this->ci->amazonprime_handler->getError())){
			$this->error = $this->ci->amazonprime_handler->getError();
		}
		
		return $product;
	}

	public function getVariationsWithAPI($asin) {
		
		if (! empty($this->error)) {
			return array(
				"error" => $this->error
			);
		} else {
			
			$requestStr = "Offers, Medium, Variations";
			$parent = $this->getParentASIN($asin);
			$single = $this->getMultiple($parent, $requestStr);
			
			if ($single[0]['inStock'] == false) {
				$parent = $asin;
			}
			
			$this->last_response = $this->getMultiple($parent, $requestStr);
			
			return $this->last_response;
		}
	}

	public function getBulk($asins) {
		
		$asinList = implode(",", $asins);
		$multiple = $this->getMultiple($asinList, "Offers, Small");
		
		return $multiple;
		
		/*
		$multipleFromProxy = [];
		
		if(is_array($multiple) && !empty($multiple)){
			foreach($multiple as $k=>$mul){
				if(isset($mul['inStock']) && $mul['inStock']==false){
					
					$multiple = $this->getVariationsWithProxy($mul['asin']);
					
					if(!empty($multiple)){
						$this->clearError();
						$multiple[$k] = $multiple[0];
					}
					
				}
			}
		}
		
		if($multiple == false){
			$multiple = $this->getVariationsWithProxy($asins[0]);
			if(!empty($multiple)){
				//$this->clearError();
				//$multiple[$asins[0]] = $multiple[0];
			}
		}
		
		return $multiple;
		*/
	}

	public function getMultiple($asin, $requestStr) {
		
		$xml = $this->amazon_query_xml($asin, $requestStr);
		if (! isset($xml->Items->Item)) {
			// $this->error = $this->error . ";;; Items->item was not set: " . PHP_EOL;
			// print_r($xml);
			return false;
		}
		
		$item = $xml->Items->Item;
		
		$out = array();
		$i = 0;
		
		if (isset($item->Variations)) {
			$varItem = $item->Variations->Item;
		}
		else {
			$varItem = $item;
		}
		
		foreach ($varItem as $var) {
			
			$out[$i]['imagesGallery'] = array();
			
			if (isset($var->ImageSets->ImageSet)) {
				$i_img_g = 0;
				foreach ($var->ImageSets->ImageSet as $imgGall) {
					$out[$i]['imagesGallery'][$i_img_g]['imageURL'] = $imgGall->LargeImage->URL->__toString();
					$i_img_g ++;
				}
			}
			
			$out[$i]['url'] = "";
			
			if(isset($var->DetailPageURL)){
				$out[$i]['url'] = $var->DetailPageURL->__toString();
				$exp_url = explode("?",$out[$i]['url']);
				if(isset($exp_url[1])){
					$out[$i]['url'] = $exp_url[0];
				}
			}
			
			$out[$i]['asin'] = $var->ASIN->__toString();
			$out[$i]['title'] = $var->ItemAttributes->Title->__toString();
			
			$out[$i]['thumbnailImage'] = isset($var->SmallImage->URL) ? $var->SmallImage->URL->__toString() : "./assets/global/img/not_available.gif";
			$out[$i]['largeImage'] = isset($var->LargeImage->URL) ? $var->LargeImage->URL->__toString() : "./assets/global/img/not_available.gif";
			
			if (isset($var->ItemAttributes->Brand)) {
				$out[$i]['more_details']['brand'] = $var->ItemAttributes->Brand->__toString();
			}
			
			
			// $out[$i]['listPrice']['formatted'] = $var->ItemAttributes->ListPrice->FormattedPrice->__toString();
			// $out[$i]['listPrice']['cents'] = $var->ItemAttributes->ListPrice->Amount->__toString();
			// $out[$i]['listPrice']['currency'] = $var->ItemAttributes->ListPrice->CurrencyCode->__toString();
			
			if (isset($var->VariationAttributes)){
				$out[$i]['variant'] = "";
				foreach ($var->VariationAttributes->VariationAttribute as $attr) {
					$out[$i]['variant'] .= $attr->Name . ": " . $attr->Value . "<br />";
				}
			}
			
			$extra = array();
			
			$lowestOffer = - 1;
			$lowestOfferPrice = PHP_INT_MAX;
			
			$lowestPrimeOffer = - 1;
			$lowestPrimeOfferPrice = PHP_INT_MAX;
			
			$j = 0;
			if (isset($var->Offers->Offer)) {
				foreach ($var->Offers->Offer as $offer) {
					
					$out[$i]['offers'][$j]['stock'] = true;
					if (isset($offer->OfferListing->Availability)) {
						$avail = strtolower($offer->OfferListing->Availability);
						foreach ($this->availOOS as $testAvail) {
							if (strpos($avail, $testAvail) !== false) {
								$out[$i]['offers'][$j]['stock'] = false;
							}
						}
						if($avail == "futuredate"){
							$extra['future_date'] = true;
						}
					}
					if(isset($offer->OfferListing->AvailabilityAttributes->AvailabilityType)){
						$avail_type = strtolower($offer->OfferListing->AvailabilityAttributes->AvailabilityType);
						foreach ($this->availOOS as $testAvail) {
							if (strpos($avail_type, $testAvail) !== false) {
								$out[$i]['offers'][$j]['stock'] = false;
							}
						}
						if($avail_type == "futuredate"){
							$extra['future_date'] = true;
						}
					}
					// $out[$i]['offers'][$j]['merchant'] = $offer->Merchant->Name->__toString();
					// $out[$i]['offers'][$j]['condition'] = $offer->OfferAttributes->Condition->__toString();
					$out[$i]['offers'][$j]['prime'] = $offer->OfferListing->IsEligibleForPrime == 1 ? true : false;
					
					$priceKey = isset($offer->OfferListing->SalePrice) ? $offer->OfferListing->SalePrice : $offer->OfferListing->Price;
					
					$out[$i]['offers'][$j]['price']['formatted'] = $priceKey->FormattedPrice->__toString();
					$out[$i]['offers'][$j]['price']['cents'] = $priceKey->Amount->__toString();
					$out[$i]['offers'][$j]['price']['currency'] = $priceKey->CurrencyCode->__toString();
					
					if ($lowestOfferPrice > $offer->OfferListing->Price->Amount) {
						$lowestOfferPrice = $offer->OfferListing->Price->Amount;
						$lowestOffer = $j;
					}
					
					if ($out[$i]['offers'][$j]['prime'] && $lowestPrimeOfferPrice > $priceKey->Amount) {
						$lowestPrimeOfferPrice = $priceKey->Amount;
						$lowestPrimeOffer = $j;
					}
					
					$j ++;
				}
				
				$extra['prime'] = $out[$i]['offers'][$lowestOffer]['prime'];
			}
			
			if (! empty($out[$i]['variant'])) {
				$extra['variant'] = $out[$i]['variant'];
			}
			
			$out[$i]['lowestPrimeOffer'] = $lowestPrimeOffer;
			$out[$i]['primePrice'] = $lowestPrimeOfferPrice;
			
			$out[$i]['lowestOffer'] = $lowestOffer;
			$out[$i]['inStock'] = $j == 0 ? false : $out[$i]['offers'][$lowestOffer]['stock'];
			
			//try to catch with proxy
			/*
			if(!isset($out[$i]['offers'])){

				$gPOWP = Amazonprime_handler::getProductOffersWithProxy($out[$i]['asin'], TRUE); //TODO second argument true is for product price without proxy  but with new condition 
				
				$out[$i]['lowestOffer'] = 0;
				$out[$i]['offers'][0]['prime'] = $gPOWP['prime'];
				$out[$i]['inStock'] = $gPOWP['stock'];
				
				if(isset($gPOWP['prime']) && $gPOWP['prime']==true){
					$out[$i]['offers'][0]['price']['formatted'] = (float) $gPOWP['primePrice'];
				} else {
					$out[$i]['offers'][0]['price']['formatted'] = (float) $gPOWP['normalPrice'];
				}
			}
			*/

			$out[$i]['extra_data'] = json_encode($extra);
			
			$i ++;
		}
		
		return $out;
	}

	public function searchProducts($page = 1, $search, $max_price, $min_price, $category, $searchType = "UPC") {
		// working on..
	}
	
	private function getSupplierHost(){
		
		$host = $this->store['url'];
		$host = str_replace("webservices.","",$host);

		return "http://".$host;
	}
	
	private function getProductPageWithScrapServer($asin, $again = true){
		
		$this->ci =& get_instance();
		
		if($this->ci->uri->segment(1) == "cron" || $this->ci->uri->segment(1) == "Cron"){
			return;
		}
		
		$this->ci->load->helper("scrap_server");
		
		$host = $this->store['url'];
		$host = str_replace("webservices.","",$host);
		$links[0] = "http://".$host."/dp/".$asin;
		
		$html = GetContentWithScrapServer($links);
		
		if(isset($html[base64_encode($links[0])])){
			
			$response = file_get_contents($html[base64_encode($links[0])]);
			
			if($again == true){
				if(strpos($response, "robot") !== false){
					$response = $this->getProductPageWithScrapServer($asin, false);	
				}
			}
			
			return $response;
		}
		
		return;
	}
	
	private function getProductPageWithProxy($asin){
		
		$this->ci = & get_instance();
		$this->ci->load->helper("curl_amazon");
		
		$host = $this->store['url'];
		$host = str_replace("webservices.","",$host);
		$url = "http://".$host."/dp/".$asin;
		
		$content = getContentAmazonWithProxy($url);
		
		if(!isset($content['result'])) return;
		
		return $content['result'];
		
		/*
		$content = '';
		
		$this->CI = & get_instance();
		$this->CI->load->model("Proxy_model", "proxy", TRUE);
		$this->CI->load->helper("ProxyCURL");
		
		if($this->CI->uri->segment(1) == "cron" || $this->CI->uri->segment(1) == "Cron"){
			return;
		}
		
		$host = $this->store['url'];
		$host = str_replace("webservices.","",$host);
		$url = "http://".$host."/dp/".$asin;
		
		
		$proxyData = $this->CI->proxy->getData();
		
		if(empty($proxyData)){
			$this->error .= "Cant get proxy list.";
		}
		
		$backData = getDataProxyCURL($proxyData['ip'], $proxyData['port'], $proxyData['username'], $proxyData['password'], $url);
		
		if ($backData['error'] == true) {
			$this->error .= "Proxy error:".$backData['error'];
			$this->CI->proxy->markErrored($proxyData['id']);
		} else {
			$content = $backData['result'];
			$content = trim($content);
		}
		
		return $content;
		*/
	}
	
	private function DOMinnerHTML(DOMNode $element){
		$innerHTML = "";
		$children  = $element->childNodes;
		
		foreach ($children as $child)
		{
			$innerHTML .= $element->ownerDocument->saveHTML($child);
		}
		
		return $innerHTML;
	} 
	
	private function getDescriptionWithProxy($content){
		
		if(empty($content)){
			return '';
		}
		
		#productDescription
		
		libxml_use_internal_errors(true);
		
		$dom = new DomDocument;
		$dom->preserveWhiteSpace = false;
		$dom->validateOnParse = true;
		
		$dom->loadHTML($content);
		
		if(empty($dom->getElementById("productDescription"))) return;
		
		$productDescription = $dom->getElementById("productDescription");
		
		return $this->DOMinnerHTML($productDescription);
		
	}
	
	private function getImagesWithProxy($content){
		
		if (!empty($content)) {
			
			$exp1 = explode('data["colorImages"] =', $content);
			if(!isset($exp1[1])){
				return NULL;
			}
			$exp2 = explode(";",$exp1[1]);
			$json = json_decode($exp2[0],TRUE);
			$variants = array();
			
			foreach($json as $var=>$details){
				foreach($details as $img){
					if(!isset($img['hiRes'])){
						continue; 
					}
					if(strpos($img['hiRes'],"http")){
						$variants[$var][] = $img['hiRes'];
					}
				}
			}
			
			if(!empty($variants)){
				return array("variants"=>$variants);
			}
			
			
			$exp1 = explode("'colorImages': { 'initial': [", $content);
	
			if(!isset($exp1[1])){
				return NULL;
			}
			
			$exp2 = explode("'colorToAsin'",$exp1[1]);
			$exp3 = str_replace(" ","",$exp2[0]);
			//$exp3 = substr($exp3,0,-4);
			//preg_match_all('@((https?://)?([-\\w]+\\.[-\\w\\.]+)+\\w(:\\d+)?(/([-\\w/_\\.]*(\\?\\S+)?)?)*)@',$exp3,$matches);
			preg_match_all('/([--:\w?@%&+~#=]*\.[a-z]{2,4}\/?)([--:\w?@%&+~#=]+|(?:[?&](?:\w+)=(?:\w+))+)?/', $exp3, $matches);
			
			$all_urls = $matches[0];
			$hd_pictures = array();
			$hd_pictures_ids = array();
			
			foreach($all_urls as $url){
				if(strpos($url,"http") !== FALSE && strpos($url,".jpg") !== FALSE){
					
					//$size = getimagesize($url);
					$exp_size = explode("_", $url);
					
					if(!isset($exp_size[1])){
						$size[0] = 600;
					}else{
						$size[0] = filter_var($exp_size[1], FILTER_SANITIZE_NUMBER_INT);
					}
					
					if($size[0] >= 450){//&& $size[1] >= 510
						$explode = explode("._",$url);
						$explode2 = explode("/",$explode[0]);
						$explode2 = end($explode2);
						$hd_pictures_ids[$explode2][$size[0]] = $url;
					}
				}
			}
			
			if(sizeof($hd_pictures_ids) < 3){
				foreach($all_urls as $url){
					if(strpos($url,"http") !== FALSE && strpos($url,".jpg") !== FALSE){
						
						//$size = getimagesize($url);
						$exp_size = explode("_", $url);
						
						if(!isset($exp_size[1])){
							$size[0] = 600;
						}else{
							$size[0] = filter_var($exp_size[1], FILTER_SANITIZE_NUMBER_INT);
						}
						
						if($size[0] >= 400){//&& $size[1] >= 400
							$explode = explode("._",$url);
							$explode2 = explode("/",$explode[0]);
							$explode2 = end($explode2);
							
							$url = str_replace("_SX466_.","_SL1466_.",$url);
							
							$hd_pictures_ids[$explode2][$size[0]] = $url;
						}
					}
				}
			}
			
			$sort_images = array();
			
			foreach($hd_pictures_ids as $pictures){
				krsort($pictures);
				foreach($pictures as $p_key=>$p_value){
					$hd_pictures[] = $p_value;
					break;
				}
			}
			
			$forUse = [];
			foreach($hd_pictures as $pic){
				if(strpos($pic,"_") !== false){
					$forUse[] = $pic;
				}
			}
			
			if(!empty($forUse)){
				return array("pictures"=>$forUse);
			}
			
		}
		
		return NULL;
	}

	public function getCategories() {
		$amazon_products = array(
			"all" => "All",
			"apparel" => "Apparel & Accessories",
			"automotive" => "Automotive",
			"baby" => "Baby",
			"beauty" => "Beauty",
			"books" => "Books",
			"photo" => "Camera & Photo",
			"wireless-phones" => "Cell Phones & Service",
			"classical" => "Classical Music",
			"pc-hardware" => "Computers",
			"videogames" => "Computer & Video Games",
			"dvd" => "DVD",
			"electronics" => "Electronics",
			"photo" => "Photo",
			"gourmet" => "Gourmet Food",
			"grocery" => "Grocery",
			"garden" => "Home & Garden",
			"hpc" => "Health & Personal Care",
			"industrial" => "Industrial & Scientific",
			"jewelry" => "Jewelry & Watches",
			"kitchen" => "Kitchen & Housewares",
			"magazines" => "Magazine Subscriptions",
			"universal" => "Miscellaneous",
			"music" => "Music",
			"mi" => "Musical Instruments",
			"software" => "Software",
			"sporting" => "Sports & Outdoors",
			"tools" => "Tools & Hardware",
			"toys" => "Toys & Games",
			"amazontv" => "Unbox Video Downloads",
			"vhs" => "VHS"
		);
		return $amazon_products;
	}

	public function parseId($idOrURL) {
		if (strlen($idOrURL) == 10) {
			return $idOrURL;
		}
		
		preg_match($this->regex, $idOrURL, $matches);
		if (count($matches) > 0) {
			return $matches[1];
		}
		else {
			return false;
		}
	}
}

<style>
table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
}

tr:nth-child(even) {
    background-color: #dddddd;
}
</style>
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/* ----------------------------- COMPLETED 12/28/17 ----------------------------- */

class RadeProxy extends CI_Controller {
function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->helper('url');
    }

	public function index(){

			$ch = curl_init();

			curl_setopt($ch, CURLOPT_URL, "https://nordvpn.com/wp-admin/admin-ajax.php?action=getProxies&searchParameters%\"5B0\"%\"5D\"%\"5Bname\"%\"5D=proxy-country&searchParameters\"%\"5B0\"%\"5D\"%\"5Bvalue\"%\"5D=&searchParameters\"%\"5B1\"%\"5D\"%\"5Bname\"%\"5D=proxy-ports&searchParameters\"%\"5B1\"%\"5D\"%\"5Bvalue\"%\"5D=&offset=0&limit=50000");
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");

			curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');

			$headers = array();
			$headers[] = "Host: nordvpn.com";
			$headers[] = "User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0";
			$headers[] = "Accept: application/json, text/javascript, */*; q=0.01";
			$headers[] = "Accept-Language: en-US,en;q=0.5";
			$headers[] = "Referer: https://nordvpn.com/free-proxy-list/";
			$headers[] = "X-Requested-With: XMLHttpRequest";
			$headers[] = "Cookie: __cfduid=d781687c98a47ee53f35a0113ee1073e01519637845; qtrans_front_language=en; PHPSESSID=rcm7kfea9bc93b0gi62noo4bn9aripqk05lseg5fsv50qvo2veq0; checkoutVariation=order; _vwo_uuid_v2=DE3ED860FFC807EB7EC9FF10499D3391F|154a8dec02a108df7aa81b7457cadc74; _vis_opt_s=1%\"7C;";
			$headers[] = "Proxy-Authorization: Digest username=\"52343300\"\",";
			$headers[] = "Connection: keep-alive";
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

			$result = curl_exec($ch);
			if (curl_errno($ch)) {
			    echo 'Error:' . curl_error($ch);
			}
			curl_close ($ch);


            $decoded = json_decode($result, true);
            $count = 0;
            $count++;
            $count_all = $this->db->count_all('proxy');

            echo "<center><h1>".$count_all." total in database</h1>proxies from nordvpn.com</center>";

	        echo "<table><tr><th>id</th><th>IP</th><th>Port</th><th>Country</th><th>Country code</th><th>Type</th></tr>";

	        $proxy_db  = array();
	       foreach($this->db->from("proxy")->get()->result_array() as $proxy){
	       		$proxy_db[$proxy['ip']."_".$proxy['port']] = $proxy;

	       }

	        $new_proxy = array();
	        
			foreach ($decoded as $o) {
	  echo	 "<tr>
			  <td>".$count++."</td>
			  <td>".$o['ip']."</td>
			  <td>".$o['port']."</td>
			  <td>".$o['country']."</td>
			  <td>".$o['country_code']."</td>
			  <td>".$o['type']."</td>
			  </tr>";

			if(!isset($proxy_db[$o['ip']."_".$o['port']])){
				  $new_proxy[] =  array(
	                'ip' => $o['ip'],
	                'port' => $o['port'],
	                'country' => $o['country'],
	                'country_code' => $o['country_code'],
	                'type' => $o['type']
	       			);
			    }
			}

			if(!empty($new_proxy)){
				 $this->db->insert_batch('proxy', $new_proxy);
			}


			echo "</table>";


	}
}
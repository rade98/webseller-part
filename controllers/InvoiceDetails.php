 
 <?php
defined('BASEPATH') OR exit('No direct script access allowed');

class InvoiceDetails extends CI_Controller {

function __construct() {
        error_reporting(0);
        parent::__construct();
        $this->load->database();
        $this->load->model('inv_model');
        $this->load->library("Inv_bg");
        $this->load->helper('url');
        if ($this->tank_auth->get_current_user()->admin != 1) {
            redirect('/account/dashboard');
            die();
        }
    }

    function index() {
       $this->load->view("tpl/header", array("title" => "Invoices"));

        $id = (int) $_GET['id'];
        $data['invoice'] = $this->inv_model->get_invoice($id);
        $user_id = $data['invoice']['user_id'];
        $data['user'] = $this->inv_model->get_user($user_id);
        $data['invoicei'] = $this->inv_model->get_invoices($user_id);
        $decode_business = $data['user']['business_settings'];
        $business_info = json_decode($decode_business, TRUE);
        $data['make_bulstat'] = preg_replace('/\D/', '', $business_info['vat_number']);
        $data['business_info'] = $business_info;
        $decode_items = $data['invoicei']['items'];
        $data['items_info'] = json_decode($decode_items, TRUE);



 
       $this->load->view('admin/invoiceDetails', $data);   

        $this->load->view("tpl/footer");
     
    }
}
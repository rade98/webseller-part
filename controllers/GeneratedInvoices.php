<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class GeneratedInvoices extends CI_Controller {
    
    public function __construct() {
        parent::__construct();
        $this->load->library("Inv_bg");
        $this->load->helper('url');
        $this->load->view("tpl/header", array("title" => "Generated invoices"));

    }

    public function index(){

        $data['invoices'] = $this->inv_bg->getInvoices();
        $this->load->view('GeneratedInvoices', $data);
    }
    public function autoGenerate()
    {
     $id = $_GET['selectedID'];

    }
     public function getClients(){
       
        $getClients = $this->Inv_bg->getClients();

            foreach ($getClients['clients'] as $client) {
                   echo($client['id'])."<br/>";
        }
     }
    public function getInvoices(){
        
        $getInvoices = $this->Inv_bg->getInvoices();

        return json_decode($getInvoices, true);
    }
    public function invoiceDetails()
    {
        $invoice = 1;
        $getInvoice = $this->Inv_bg->invoiceDetails($invoice);
    }
    public function clientDetails()
    {
        $client = 1;
        $getClient = $this->Inv_bg->clientDetails($client);
    }
      function __destruct()
    {


                $this->load->view("tpl/footer");

    }
}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Shopify extends CI_Controller {

public function __construct() {
		
    parent::__construct();
    $this->load->model('shopify_model','shopify_model');
    $this->load->helper('url');
    $this->load->config("shopify");
    $this->load->library("Shopify_handler",array(
    	"API_KEY"=> $this->config->item("SHOPIFY_API_KEY"),
    	"API_SECRET_KEY"=> $this->config->item("SHOPIFY_API_SECRET_KEY"),
    	"REFRESH_TOKEN"=> $this->config->item("SHOPIFY_REFRESH_TOKEN"),
    	"DOMAIN_SERVER"=> $this->config->item("SHOPIFY_DOMAIN_SERVER"),
    	"STORE"=>"rade-pro"
    ));
    if (! $this->tank_auth->is_logged_in()) {
    	redirect("auth/login");
    }
}
public function UserInfo(){
    $get_user_id = $this->tank_auth->get_current_user()->id;
    $this->load->view("tpl/header", array("title" => "Shopify Accounts"));
    $data['accounts'] = $this->shopify_model->get_shopify_accounts($get_user_id);
    $this->load->library('form_validation');
    $this->form_validation->set_rules('ShopifyStore', 'ShopifyStore','required|max_length[255]');
    if($this->form_validation->run())
      {   
    $params = array(
      'key_data' => '{"store_name":"'.$this->input->post('ShopifyStore').'"',
      'user_id' => $this->tank_auth->get_current_user()->id,
      'store_id' => 100
       );
    $Shopify_id = $this->shopify_model->add_account($params);
      redirect('Shopify/RedirectUser?store='.$this->input->post('ShopifyStore'));
      }
    else
      {            
    $this->load->view("link/shopify_accounts", $data);
      } 
$this->load->view("tpl/footer");
	}
public function RedirectUser(){
    $shop = $_GET['store'];
    $api_key = "812f7474df85033e4430baaec308e4b3";
    $scopes = "read_orders,write_products";
    $redirect_uri = base_url()."Shopify/GenerateUser";
    $install_url = "https://" . $shop . ".myshopify.com/admin/oauth/authorize?client_id=" . $api_key . "&scope=" . $scopes . "&redirect_uri=" . urlencode($redirect_uri);
    redirect($install_url);
die();	
 }
public function GenerateUser(){
    $api_key = "812f7474df85033e4430baaec308e4b3";
    $shared_secret = "f8cbf4815c2cdd953ad05fab0fb0c4b0";
    $params = $_GET;
    $hmac = $_GET['hmac'];
    $params = array_diff_key($params, array('hmac' => ''));
    ksort($params);
    $computed_hmac = hash_hmac('sha256', http_build_query($params), $shared_secret);
    if (hash_equals($hmac, $computed_hmac)) {
    	$query = array(
    		"client_id" => $api_key,
    		"client_secret" => $shared_secret,
    		"code" => $params['code']
    );
    $access_token_url = "https://" . $params['shop'] . "/admin/oauth/access_token";
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_URL, $access_token_url);
    curl_setopt($ch, CURLOPT_POST, count($query));
    curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($query));
    $result = curl_exec($ch);
    curl_close($ch);
    $result = json_decode($result, true);
    $access_token = $result['access_token'];
    $get_code = $_GET['code'];
    $get_hmac = $_GET['hmac'];
    $get_shop = $_GET['shop'];
    $get_time = $_GET['timestamp'];
    $params = '"apikey":"'.$access_token.'", "code":"'.$get_code.'","hmac":"'.$get_hmac.'","shoplink":"'.$get_shop.'","shopifytime":"'.$get_time.'"}';
    $Shopify_id = $this->shopify_model->account_finish($params);
    redirect('Shopify/UserInfo');
    } else {
    	die('This request is NOT from Shopify!');
	}
  }
}
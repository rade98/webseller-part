<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SingleGenerateInvoice extends CI_Controller {

function __construct() {
    error_reporting(0);
        parent::__construct();
        $this->load->database();
        $this->load->model('inv_model');
        $this->load->library("Inv_bg");
        $this->load->helper('url');
        $this->load->view("tpl/header", array("title" => "Generated details"));

    }
    function index() {
        $id = (int) $_GET['id'];
        $inv_number = $_GET['selectedId'];
        $this->generateInvoice($id, $inv_number);

       echo  $inv_number;
       }

    public function generateInvoice($id,$inv_number, $response){


        $data['invoice'] = $this->inv_model->get_invoice($id);

        if($data['invoice']['generated']==1){
            return;
        }

        echo $inv_number;


        $user_id = $data['invoice']['user_id'];
        $exist_id = $data['invoice']['invoice_id'];
        $data['user'] = $this->inv_model->get_user($user_id);
        $data['invoicei'] = $this->inv_model->get_invoices($exist_id);
        $decode_business = $data['user']['business_settings'];
        $business_info = json_decode($decode_business, TRUE);
        $decode_items = $data['invoicei']['items'];
        $items_info = json_decode($decode_items, TRUE);
        $invoice_data = array();
        $make_bulstat = preg_replace('/\D/', '', $business_info['vat_number']);
        $invoice_data['to_firm_bulstat'] = $make_bulstat;
        
        if($business_info['issuer'] == NULL || $business_info['business_name'] == NULL || $business_info['city'] == NULL || $business_info['address'] == NULL){
           $invoice_data['to_firm_vat_number'] = "";
            $invoice_data['to_firm_name'] = "";
        }else{
            $invoice_data['to_firm_vat_number'] = $business_info['vat_number'];
            $invoice_data['to_firm_name'] = $business_info['business_name'];
        }

        $invoice_data['status'] = $data['invoicei']['status'];
        $invoice_data['vat_percent'] = 0;
        $invoice_data['payment_method'] = "bank";
        $invoice_data['without_vat_reason'] = "чл.113 ал.9 от ЗДДС";
        $invoice_data['inv_number'] = $inv_number;
        $invoice_data['to_firm_mol'] = $business_info['issuer'];
        $invoice_data['to_firm_town'] = $business_info['city'];
        $invoice_data['to_firm_addr'] = $business_info['address'];
        $convertTime = strtotime($data['invoice']['date']);
        $convertTime2 = strtotime($data['invoice']['date']);
        $invoice_data['date_create'] = $convertTime;
        $invoice_data['date_event'] = $convertTime2;
       
        $items = array();
        foreach ($items_info as $item) {
        if($item['price'] == 0){
        }else{
            $itemReady['name'] = $item['name'];
            $itemReady['quantity'] = $item['qty'];
            $itemReady['quantity_unit'] = 'бр.';
            
            $itemReady['price'] = $item['price'];
            $itemReady['price_for_quantity'] = '1';

            $items[] = $itemReady;
        }
    }
    
        $invoice_data['payment_currency'] = 'USD';
        $invoice_data['items'] =  $items;
        $invoice_data['name'] = "test";
      

        
        $response = $this->inv_bg->addInvoice($invoice_data);

        echo "<br /><br />";


        if($response['error']['code'] == 400){
            $this->db->set('error', '1');
            $this->db->set('error_message', $response['error']['description']);
            $this->db->where('id', $id);
            $this->db->update('invoices_requests');
          $error_message = $response['error']['description'];
      $this->session->set_flashdata('my_response', '<div class="alert alert-danger"><strong>Danger!</strong> '.$error_message.' <a href="'.base_url().'InvoiceErrors">Error list</a></div>');
         // echo $get_error_messages." ID objave:".$id;
        } else {
            $this->db->set('generated', '1', FALSE);
            $this->db->set('original_invoice_id', $response['success']['id'], FALSE);
            $this->db->where('id', $id);
            $this->db->update('invoices_requests');
            $success_message = $response['success']['message'];
            $this->session->set_flashdata('my_response', '<div class="alert alert-success"><strong>Great!</strong> '.$success_message.' <a href="'.base_url().'/GeneratedInvoices">Generated list</a></div>');
 foreach ($response as $getm) {
           $get_success_message = $getm['message'].",";
        }

         echo $get_success_message;
         $show_it = 3;
         $this->load->view('generatedInvoicesNoti', $show_it);


var_dump($response);
        }
              redirect('/invoiceRequests');

       $this->load->view("tpl/footer");

    }

}
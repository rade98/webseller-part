<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jobs extends CI_Controller {

function __construct()
    {
        parent::__construct();
        $this->load->model('jobs_model','jobs_model');
        $this->load->helper('url');
        
    } 

    /*
     * Listing of Jobs
     */
    function index()
    {
    	if ($this->tank_auth->get_current_user()->admin != 1) {
            redirect('/account/dashboard');
            die();
        }
       if(in_array($this->tank_auth->get_current_user()->user_level, array("admin","developer")) == false){
            show_missing_permission();
            return;
        }
    	
        $data['jobs'] = $this->jobs_model->get_all_Jobs();
        
        $this->load->view("tpl/header", array("title" => "Jobs list"));
        $this->load->view('admin/listJobs', $data);
        $this->load->view("tpl/footer");
    }
    function Careers()
    {
    	 $data['jobs'] = $this->jobs_model->get_all_Jobs();
        
		$this->load->view("home/tpl/header", array("title" => "Careers"));
        $this->load->view('home/careers', $data);
		$this->load->view("home/tpl/footer");
    }
    /*
     * Adding a new Jobs
     */
    function add()
    {   
    	if ($this->tank_auth->get_current_user()->admin != 1) {
            redirect('/account/dashboard');
            die();
        }

    	if(in_array($this->tank_auth->get_current_user()->user_level, array("admin","developer")) == false){
			show_missing_permission();
			return;
		}

        $this->load->library('form_validation');

		$this->form_validation->set_rules('JobTitle','JobTitle','required|max_length[255]');
		$this->form_validation->set_rules('JobContent','JobContent','required');
		$this->form_validation->set_rules('JobCategory','JobCategory','required|max_length[255]');

		
		if($this->form_validation->run())     
        {   
            $params = array(
				'JobTitle' => $this->input->post('JobTitle'),
				'JobContent' => $this->input->post('JobContent'),
				'JobCategory' => $this->input->post('JobCategory'),
            );
            
            $Jobs_id = $this->jobs_model->add_Jobs($params);
            redirect('Jobs/index');
        }
        else
        {            
            $this->load->view("tpl/header", array("title" => "Add job"));
            $this->load->view('admin/addJob');
            $this->load->view("tpl/footer");
        }
    }  

    /*
     * Editing a Jobs
     */
    function edit($ID)
    {   
    	if ($this->tank_auth->get_current_user()->admin != 1) {
            redirect('/account/dashboard');
            die();
        }
        // check if the Jobs exists before trying to edit it
        $data['Jobs'] = $this->jobs_model->get_Jobs($ID);
        
        if(isset($data['Jobs']['ID']))
        {
            $this->load->library('form_validation');

			$this->form_validation->set_rules('JobTitle','JobTitle','required|max_length[255]');
            $this->form_validation->set_rules('JobContent','JobContent','required');
            $this->form_validation->set_rules('JobCategory','JobCategory','required|max_length[255]');
		
			if($this->form_validation->run())     
            {   
                $params = array(
					'JobTitle' => $this->input->post('JobTitle'),
                    'JobContent' => $this->input->post('JobContent'),
                    'JobCategory' => $this->input->post('JobCategory'),
                );

                $this->jobs_model->update_Jobs($ID,$params);            
                redirect('Jobs/index');
            }
            else
            {
                $data['_view'] = 'Jobs/edit';
                $this->load->view("tpl/header", array("title" => "Edit Job"));
                $this->load->view('admin/editJob',$data);
                $this->load->view("tpl/footer");
            }
        }
        else
            show_error('The Jobs you are trying to edit does not exist.');
    } 

    /*
     * Deleting Jobs
     */
    function remove($ID)
    {
    	if ($this->tank_auth->get_current_user()->admin != 1) {
            redirect('/account/dashboard');
            die();
        }
        $Jobs = $this->jobs_model->get_Jobs($ID);

        // check if the Jobs exists before trying to delete it
        if(isset($Jobs['ID']))
        {
            $this->jobs_model->delete_Jobs($ID);
            redirect('Jobs/index');
        }
        else
            error('The Jobs you are trying to delete does not exist.');
    }
    
}

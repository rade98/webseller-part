<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class InvoiceArchive extends CI_Controller {

function __construct()
    {
        parent::__construct();
        $this->load->model('inv_model');
        $this->load->library("Inv_bg");
        $this->load->database();
        $this->load->helper('url');
        if ($this->tank_auth->get_current_user()->admin != 1) {
            redirect('/account/dashboard');
            die();
        }
    } 

    /*
     * Listing of ourteam
     */
    function index()
    {
      

        $data['Invoices'] = $this->inv_model->get_deleted_Invoices();
        

        $params = array();
        $limit_per_page = 25;
        $page = ($this->uri->segment(3)) ? ($this->uri->segment(3) - 1) : 0;
        
        $total_records = 50;

        $config['total_rows'] = $total_records;
        $config['per_page'] = $limit_per_page;
        $config["uri_segment"] = 3;
            
        // custom paging configuration
        $config['base_url'] = base_url() . 'invoiceRequests';
        $config['num_links'] = 10;
        $config['use_page_numbers'] = TRUE;
        $config['reuse_query_string'] = TRUE;



        $this->load->library('pagination');
        $this->pagination->initialize($config);
            
        // build paging links
        $data["links"] = $this->pagination->create_links();
        
        $data['error'] = $this->session->flashdata('error');

        $data['_view'] = 'Invoices/index';
        $this->load->view("tpl/header", array("title" => "Invoices"));
        $this->load->view('admin/listInvoices', $data);
        $this->load->view("tpl/footer");
    }
    public function getFirstGroup()
    {
        $data['invoices'] = $this->inv_bg->getInvoices();
    }
   function remove($id)
    {
        $Invoice = $this->inv_model->check_invoice($id);

        if(isset($Invoice['id']))
        {
            $this->inv_model->archive_invoice($id);
            redirect('invoiceRequests/index');
        }
        else
            error('The Jobs you are trying to delete does not exist.');
    }
}
